#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from numpy import arange, pi
import abc
from collections import namedtuple

Coefs = namedtuple("Coefs", "l R Toc F0 x_step a0 aN k0 n theta kN")

class SystemSolver(metaclass=abc.ABCMeta):

    def __init__(self):
        pass

    def set_all_coefs(self, coefs):
        self.l = coefs.l
        self.R = coefs.R
        self.Toc = coefs.Toc
        self.F0 = coefs.F0
        self.x_step = coefs.x_step
        self.a0 = coefs.a0
        self.aN = coefs.aN
        self.k0 = coefs.k0
        self.n = coefs.n
        self.theta = coefs.theta
        self.kN = coefs.kN

    @property
    def l(self):
        return self.__l
    
    @l.setter
    def l(self, value):
        self.__l = value

    @property
    def R(self):
        return self.__R

    @R.setter
    def R(self, value):
        self.__R = value

    @property
    def Toc(self):
        return self.__Toc
    
    @Toc.setter
    def Toc(self, value):
        self.__Toc = value

    @property
    def F0(self):
        return self.__F0
    
    @F0.setter
    def F0(self, value):
        self.__F0 = value
    @property
    def x_step(self):
        return self.__x_step

    @x_step.setter
    def x_step(self, value):
        self.__x_step = value

    @property
    def a0(self):
        return self.__a0
    
    @a0.setter
    def a0(self, value):
        self.__a0 = value

    @property
    def aN(self):
        return self.__aN

    @aN.setter
    def aN(self, value):
        self.__aN = value
    
    @property
    def k0(self):
        return self.__k0
    
    @k0.setter
    def k0(self, value):
        self.__k0 = value

    @property
    def n(self):
        return self.__n

    @n.setter
    def n(self, value):
        self.__n = value
        
    @property
    def theta(self):
        return self.__theta
    
    @theta.setter
    def theta(self, value):
        self.__theta = value
    
    @property
    def kN(self):
        return self.__kN
    
    @kN.setter
    def kN(self, value):
        self.__kN = value

    def test_filling(self, low, high, step):
        res_x = []
        res_T = []
        for x in arange(low, high, step):
            res_x.append(x)
            res_T.append(x ** 2)
        return res_x, res_T

    @abc.abstractmethod
    def solve_system(self):
        raise NotImplementedError()


class TridiagonalMatrixSystemSolver(SystemSolver):

    EPS = 10**-6

    def __init__(self):
        self.__a_a = lambda : self.aN * self.l / (self.aN - self.a0)
        self.__a_k = lambda : -self.k0* self.kN*self.l / (self.kN - self.k0)
        self.__b = lambda : self.kN * self.l / (self.kN - self.k0)
        self.__c = lambda : - self.a0 * self.__a_a()
        self.__alpha = lambda x, a_a, c : c / (x - a_a)
        self.__p = lambda x, alpha : 2 / self.R * alpha
        self.__f = lambda x, alpha : 2 / self.R * alpha * self.Toc
        self.__k = lambda x, a_k, b : a_k / (x-b)
#        self.__k = lambda T : self.k0 * (T/self.theta) ** self.n
        self.__kappa = lambda k_cur, k_next: 2 * k_cur * k_next / (k_cur + k_next)
        self.__A = lambda k_prev, k_cur : self.__kappa(k_prev, k_cur)
        self.__C = lambda k_cur, k_next : self.__kappa(k_cur, k_next)
        self.__B = lambda A, C, p_cur : A + C + p_cur*self.x_step**2
        self.__F = lambda f_cur : f_cur * self.x_step**2
        self.__ksi_next = lambda A, B, C, ksi_cur : C / (B - A * ksi_cur)
        self.__eta_next = lambda A, B, F, ksi_cur, eta_cur : (F + A * eta_cur) / (B - A * ksi_cur)
        self.__K0 = lambda kappa, p0, p1 : kappa + self.x_step**2 / 8 *(p0 + p1) / 2 + self.x_step**2 / 4 * p0
        self.__M0 = lambda kappa, p0, p1 : self.x_step/8 * (p0 + p1) / 2 - kappa
        self.__P0 = lambda f0, f1 : self.x_step * self.F0 + self.x_step ** 2 / 4 *((f0+f1)/2 + f0)
        self.__ksi_1 = lambda K0, M0 : - M0 / K0
        self.__eta_1 = lambda K0, P0 : P0 / K0
        self.__x = []
        self.__KN = lambda kappa, p_N_1, p_N :  kappa - self.x_step ** 2 / 16 * (p_N_1 + p_N)
        self.__MN = lambda  alpha, kappa, p_N_1, p_N: -self.x_step * alpha - kappa - self.x_step**2/4 * p_N - self.x_step**2/16 * (p_N_1 + p_N)
        self.__PN = lambda alpha, f_N_1, f_N : -self.x_step**2/4 *(f_N + (f_N_1 + f_N)/2) - self.x_step * alpha * self.Toc
        self.__yN = lambda KN, MN, PN, ksiN, etaN : (PN - KN * etaN) / (KN*ksiN + MN)
        self.__y_n_1 = lambda ksi_n, eta_n, y_n : ksi_n * y_n + eta_n

    def get_ksi_eta(self):
        self.a_a, self.a_k, self.b, self.c  = self.__a_a(), self.__a_k(), self.__b(), self.__c()
        a_a, a_k,b,c = self.a_a, self.a_k, self.b, self.c
        x, max_x = 0, self.l + self.x_step
        self.__x = [x,]
        k1 = self.__k(x+self.x_step, a_k, b)
        kappa = self.__kappa(self.k0, k1)
        p0 = self.__p(x, self.a0)
        alpha = self.__alpha(x + self.x_step, a_a, c)
        p1 = self.__p(x + self.x_step, alpha)
        K0 = self.__K0(kappa, p0, p1)
        M0 = self.__M0(kappa, p0, p1)
        f0 = self.__f(x, self.a0)
        f1 = self.__f(x+self.x_step, alpha)
        P0 = self.__P0(f0, f1)
        ksi =[
            self.__ksi_1(K0, M0)
        ]
        eta = [
            self.__eta_1(K0, P0)
        ]
        x += self.x_step
        k_prev = self.k0
        k_cur = k1
        k_next = self.__k(x + self.x_step, a_k, b)
        while x < max_x:
            self.__x.append(x)
            alpha = self.__alpha(x, a_a, c)
            A = self.__A(k_prev, k_cur)
            C = self.__C(k_cur, k_next)
            B = self.__B(A, C, self.__p(x, alpha))
            F = self.__F(self.__f(x, alpha))
            ksi.append(self.__ksi_next(A, B, C, ksi[-1]))
            eta.append(self.__eta_next(A, B, F, ksi[-2], eta[-1]))
            x += self.x_step
            k_next = self.__k(x+self.x_step, a_k, b)
            k_cur = k_next
            k_prev = k_cur
        return (ksi, eta)

    def __compute_T(self, ksi, eta):
        k_N_1 = self.__k(self.__x[-2], self.a_k, self.b)
        k_N = self.__k(self.__x[-1], self.a_k, self.b)
        alpha_N = self.__alpha(self.__x[-1], self.a_a, self.c)
        alpha_N_1 =  self.__alpha(self.__x[-2], self.a_a, self.c)
        kappa = self.__kappa(k_N_1, k_N)
        p_N_1 = self.__p(self.__x[-2], alpha_N_1)
        p_N = self.__p(self.__x[-1], alpha_N)
        f_N_1 = self.__f(self.__x[-2], alpha_N_1)
        f_N = self.__f(self.__x[-1], alpha_N)
        KN = self.__KN(kappa, p_N_1, p_N)
        MN = self.__MN(alpha_N, kappa, p_N_1, p_N)
        PN = self.__PN(alpha_N, f_N_1, f_N)
        T = [self.__yN(KN, MN, PN, ksi[-1], eta[-1])]
        max_ind = len(ksi)-1
        ind = max_ind
        while ind > 0:
            y_n_1 = self.__y_n_1(ksi[ind], eta[ind], T[0])
            T.insert(0, y_n_1)
            ind -= 1
        return T

    def solve_system(self):
        ksi, eta = self.get_ksi_eta()
        result = (self.__x, self.__compute_T(ksi, eta))
        return result
