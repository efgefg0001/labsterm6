#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib
matplotlib.use("Qt5Agg")
from PyQt5 import QtWidgets, QtGui, QtCore, uic
from PyQt5.QtWidgets import QHBoxLayout, QSizePolicy, QMessageBox, QWidget
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

from systemsolver import *

FormClass = uic.loadUiType('mainwindow.ui')

class MainWindow(FormClass[1], FormClass[0]):

    def __init__(self, parent=None):
        FormClass[1].__init__(self, parent)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setupUi(self)
        self.verticalLayout.addLayout(self.__create_plots())
        self.solveButton.clicked.connect(self.onClickedSolveButton)

        self.__system = TridiagonalMatrixSystemSolver()

    def __create_plots(self):
        self.__T_x_plot = MplCanvas("x", "T", 20)
        horiz = QHBoxLayout()
        horiz.addWidget(self.__T_x_plot)
        return horiz

    def __fill_data_for_plots(self, data):
        self.__T_x_plot.load_data(data[0], data[1])

    def clear_all_plots(self):
        self.__T_x_plot.clear_plot()

    def onClickedSolveButton(self):
        self.clear_all_plots()
        try:
            coefs = Coefs(
                l=float(self.lenLineEdit.text()),
                R=float(self.rLineEdit.text()),
                Toc=float(self.tocLineEdit.text()),
                F0=float(self.f0LineEdit.text()),
                x_step=float(self.hLineEdit.text()),
                a0=float(self.a0LineEdit.text()),
                aN=float(self.aNLineEdit.text()),
                k0=float(self.k0LineEdit.text()),
                kN=float(self.kNLineEdit.text()),
                n=1, #float(self.nLineEdit.text()),
                theta=1, #float(self.thetaLineEdit.text()),
            )
            self.__system.set_all_coefs(coefs)
            result = self.__system.solve_system()
            self.__fill_data_for_plots(result)
        except Exception as error:
            QtWidgets.QMessageBox.warning(self, "Lab3",
                                        str(error), QtWidgets.QMessageBox.Ok)


class MplCanvas(FigureCanvas):

    def __init__(self, xlabel, ylabel, fontsize, parent=None, width=5, height=4, dpi=70):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        self.axes.hold(True)
#        self.draw_plot()
        FigureCanvas.__init__(self, fig)
        self.setParent(parent)
        FigureCanvas.setSizePolicy(self,
                QSizePolicy.Expanding,
                QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        self.__xlabel = xlabel
        self.__ylabel = ylabel
        self.__fontsize = fontsize
        self.axes.set_xlabel(xlabel, fontsize=self.__fontsize)
        self.axes.set_ylabel(ylabel, rotation=0, fontsize=self.__fontsize)
        self.__color_f = "r"

    def load_data(self, x, y):
        self.__x = x
        self.__y = y
        self.draw_plot()

    def clear_plot(self):
#        self.axes.cla()
        self.axes.clear()
        self.draw()

    def draw_plot(self):
        self.axes.set_xlabel(self.__xlabel, fontsize=self.__fontsize)
        self.axes.set_ylabel(self.__ylabel, rotation=0, fontsize=self.__fontsize)
        self.axes.plot(self.__x, self.__y, self.__color_f)
        self.draw()

