#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from numpy import arange, pi
import abc
from collections import namedtuple
import time

import pyximport; pyximport.install()
#from fastsyssolver import *
from  fastsyssolver_af import *

Coefs = namedtuple("Coefs", "l R Toc F0 h a0 aN k0 Theta c0 T p m tau eps sigma numOfOut")

class SystemSolver(metaclass=abc.ABCMeta):

    def __init__(self):
        pass

    @property
    def coefs(self):
        return self.__coefs
    
    @coefs.setter
    def coefs(self, value):
        self.__coefs = value

    @abc.abstractmethod
    def solve_system(self):
        raise NotImplementedError()


class TridiagonalMatrixSystemSolver(SystemSolver):

    EPS = 10**-6

    def __init__(self):
        pass
        """
        self.__a_a = lambda : self.aN * self.l / (self.aN - self.a0)
        self.__a_k = lambda : -self.k0* self.kN*self.l / (self.kN - self.k0)
        self.__b = lambda : self.kN * self.l / (self.kN - self.k0)
        self.__c = lambda : - self.a0 * self.__a_a()
        self.__alpha = lambda x, a_a, c : c / (x - a_a)
        self.__p = lambda x, alpha : 2 / self.R * alpha
        self.__f = lambda x, alpha : 2 / self.R * alpha * self.Toc
        self.__k = lambda x, a_k, b : a_k / (x-b)
#        self.__k = lambda T : self.k0 * (T/self.Theta) ** self.n
        self.__kappa = lambda k_cur, k_next: 2 * k_cur * k_next / (k_cur + k_next)
        self.__A = lambda k_prev, k_cur : self.__kappa(k_prev, k_cur)
        self.__C = lambda k_cur, k_next : self.__kappa(k_cur, k_next)
        self.__B = lambda A, C, p_cur : A + C + p_cur*self.h**2
        self.__F = lambda f_cur : f_cur * self.h**2
        self.__ksi_next = lambda A, B, C, ksi_cur : C / (B - A * ksi_cur)
        self.__eta_next = lambda A, B, F, ksi_cur, eta_cur : (F + A * eta_cur) / (B - A * ksi_cur)
        self.__K0 = lambda kappa, p0, p1 : kappa + self.h**2 / 8 *(p0 + p1) / 2 + self.h**2 / 4 * p0
        self.__M0 = lambda kappa, p0, p1 : self.h/8 * (p0 + p1) / 2 - kappa
        self.__P0 = lambda f0, f1 : self.h * self.F0 + self.h ** 2 / 4 *((f0+f1)/2 + f0)
        self.__ksi_1 = lambda K0, M0 : - M0 / K0
        self.__eta_1 = lambda K0, P0 : P0 / K0
        self.__x = []
        self.__KN = lambda kappa, p_N_1, p_N :  kappa - self.h ** 2 / 16 * (p_N_1 + p_N)
        self.__MN = lambda  alpha, kappa, p_N_1, p_N: -self.h * alpha - kappa - self.h**2/4 * p_N - self.h**2/16 * (p_N_1 + p_N)
        self.__PN = lambda alpha, f_N_1, f_N : -self.h**2/4 *(f_N + (f_N_1 + f_N)/2) - self.h * alpha * self.Toc
        self.__yN = lambda KN, MN, PN, ksiN, etaN : (PN - KN * etaN) / (KN*ksiN + MN)
        self.__y_n_1 = lambda ksi_n, eta_n, y_n : ksi_n * y_n + eta_n
        """

    def get_ksi_eta(self):
        """
        self.a_a, self.a_k, self.b, self.c  = self.__a_a(), self.__a_k(), self.__b(), self.__c()
        a_a, a_k,b,c = self.a_a, self.a_k, self.b, self.c
        x, max_x = 0, self.l + self.h
        self.__x = [x,]
        k1 = self.__k(x+self.h, a_k, b)
        kappa = self.__kappa(self.k0, k1)
        p0 = self.__p(x, self.a0)
        alpha = self.__alpha(x + self.h, a_a, c)
        p1 = self.__p(x + self.h, alpha)
        K0 = self.__K0(kappa, p0, p1)
        M0 = self.__M0(kappa, p0, p1)
        f0 = self.__f(x, self.a0)
        f1 = self.__f(x+self.h, alpha)
        P0 = self.__P0(f0, f1)
        ksi =[
            self.__ksi_1(K0, M0)
        ]
        eta = [
            self.__eta_1(K0, P0)
        ]
        x += self.h
        k_prev = self.k0
        k_cur = k1
        k_next = self.__k(x + self.h, a_k, b)
        while x < max_x:
            self.__x.append(x)
            alpha = self.__alpha(x, a_a, c)
            A = self.__A(k_prev, k_cur)
            C = self.__C(k_cur, k_next)
            B = self.__B(A, C, self.__p(x, alpha))
            F = self.__F(self.__f(x, alpha))
            ksi.append(self.__ksi_next(A, B, C, ksi[-1]))
            eta.append(self.__eta_next(A, B, F, ksi[-2], eta[-1]))
            x += self.h
            k_next = self.__k(x+self.h, a_k, b)
            k_cur = k_next
            k_prev = k_cur
        return (ksi, eta)
        """
        pass

    def __compute_T(self, ksi, eta):
        """
        k_N_1 = self.__k(self.__x[-2], self.a_k, self.b)
        k_N = self.__k(self.__x[-1], self.a_k, self.b)
        alpha_N = self.__alpha(self.__x[-1], self.a_a, self.c)
        alpha_N_1 =  self.__alpha(self.__x[-2], self.a_a, self.c)
        kappa = self.__kappa(k_N_1, k_N)
        p_N_1 = self.__p(self.__x[-2], alpha_N_1)
        p_N = self.__p(self.__x[-1], alpha_N)
        f_N_1 = self.__f(self.__x[-2], alpha_N_1)
        f_N = self.__f(self.__x[-1], alpha_N)
        KN = self.__KN(kappa, p_N_1, p_N)
        MN = self.__MN(alpha_N, kappa, p_N_1, p_N)
        PN = self.__PN(alpha_N, f_N_1, f_N)
        T = [self.__yN(KN, MN, PN, ksi[-1], eta[-1])]
        max_ind = len(ksi)-1
        ind = max_ind
        while ind > 0:
            y_n_1 = self.__y_n_1(ksi[ind], eta[ind], T[0])
            T.insert(0, y_n_1)
            ind -= 1
        return T
        """
        pass
    def range(self, min, max, step):
        item = min
        res_list = []
        while item < max:
            res_list.append(item)
            item += step
        return res_list

    def solve_system(self):
        t1 = time.process_time()
        """
        times = self.range(0, self.coefs.T, self.coefs.tau)
        times_out = []
        xs = self.range(0, self.coefs.l, self.coefs.h)
        temprs = []
        temprs_out = []
        for i, t in enumerate(times):
            temprs_for_fixed_t = get_tempr_for_time(t, xs, self.coefs)
            temprs.append(temprs_for_fixed_t)
            if i % self.coefs.numOfOut == 0:
                times_out.append(t)
                temprs_out.append(temprs_for_fixed_t)
        """
        times_out, xs, temprs_out = solve_system(self.coefs)
        t2 = time.process_time()
        print("dur = {}".format(t2 - t1))
        return np.array(times_out), np.array(xs), np.array(temprs_out)
#        ksi, eta = self.get_ksi_eta()
#        result = (self.__x, self.__compute_T(ksi, eta))
#        return result
