#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from PyQt4 import QtGui, QtCore, uic
from PyQt4.QtGui import QHBoxLayout, QVBoxLayout

import pyqtgraph as pg
import pyqtgraph.opengl as gl

import numpy as np

from systemsolver import *

FormClass = uic.loadUiType('mainwindow.ui')


class MainWindow(FormClass[1], FormClass[0]):

    def __init__(self, parent=None):
        FormClass[1].__init__(self, parent)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setupUi(self)
        self.viewWidget = self.__create_view_widget()
        self.horizontalLayout.addWidget(self.viewWidget)
#        self.horizontalLayout.addWidget(QtGui.QPushButton("button"))
        self.solvePushButton.clicked.connect(self.onClickedSolvePushButton)
        self.setSmoothPushButton.clicked.connect(self.onClickedSetSmoothPushButton)
        self.__system_solver = TridiagonalMatrixSystemSolver()
        self.__min_tempr = 0
        self.__scale = QtGui.QVector3D(1, 1, 1)
        self.X_SIZE = 13.5
        self.Y_SIZE = 13.5
        self.Z_SIZE = 16


    def onClickedSetSmoothPushButton(self):
        prev = self.__data.opts["smooth"]
        cur = prev
        if self.smoothOnRadioButton.isChecked():
            cur = True
        else:
            cur = False
        if prev != cur:
            self.__data.opts["smooth"] = cur
            self.__data.meshDataChanged()


    def __create_view_widget(self):
        view_widget = gl.GLViewWidget()
        view_widget.opts['distance'] = 40
        view_widget.addItem(gl.GLAxisItem(size=QtGui.QVector3D(1000,1000,1000)))
        self.__data = gl.GLSurfacePlotItem(x=None, y=None, z=None,
                                           shader='shaded',
                                           smooth=False,
                                           computeNormals=True)
        view_widget.addItem(self.__data)
        return view_widget

    def __get_max_tempr(self, temprs):
        max_tempr = 0
        for tempr_list in temprs:
            for tempr in tempr_list:
                if tempr > max_tempr:
                    max_tempr = tempr
        return max_tempr

    def __get_min_tempr(self, temprs):
        min_tempr = temprs[0][0]
        for tempr_list in temprs:
            for tempr in tempr_list:
                if tempr < min_tempr:
                    min_tempr = tempr
        return min_tempr


    def setData(self, x, y, z):
        max_tempr = self.__get_max_tempr(z)
        self.__data.translate(0, 0, self.__min_tempr, True)
        self.__data.scale(1/self.__scale.x(), 1/self.__scale.y(), 1/self.__scale.z())
#        self.__data.scale(1/self.__scale.x(), 1/self.__scale.y(), 1/self.__scale.z())
        self.__min_tempr = self.__get_min_tempr(z)
        size = self.viewWidget.size()
        self.__scale.setX(self.X_SIZE / x[-1])
        self.__scale.setY(self.Y_SIZE/ y[-1])
        dif=(max_tempr - self.__min_tempr)
        if abs(dif) < 1e-6:
            self.__scale.setZ(1)
        else:
            self.__scale.setZ(self.Z_SIZE/ dif)
        self.__data.setData(x, y, z)
#        self.__data.translate(0, 0, -self.__min_tempr)
#        print("size = {}".format(self.viewWidget.size()))
        self.__data.scale(self.__scale.x(), self.__scale.y(), self.__scale.z())#self.__scale.z())
        self.__data.translate(0, 0, -self.__min_tempr, True)
        print("min tempr = {}, max tempr = {}".format(self.__min_tempr, max_tempr))

    def __fill_data_for_plots(self, data):
        self.__T_x_plot.load_data(data[0], data[1])

    def clear_all_plots(self):
        self.setData(None, None, None)

    def onClickedSolvePushButton(self):
#        self.clear_all_plots()
        try:
            coefs = {
                "l": float(self.lLineEdit.text()),
                "R": float(self.RLineEdit.text()),
                "Toc":float(self.TocLineEdit.text()),
                "F0":float(self.F0LineEdit.text()),
                "h":float(self.hLineEdit.text()),
                "a0":float(self.a0LineEdit.text()),
                "aN":float(self.aNLineEdit.text()),
                "k0":float(self.k0LineEdit.text()),
                "Theta":float(self.ThetaLineEdit.text()),
                "T":float(self.TLineEdit.text()),
                "m":int(self.mLineEdit.text()),
                "p":int(self.pLineEdit.text()),
                "eps":float(self.epsLineEdit.text()),
                "tau":float(self.tauLineEdit.text()),
                "c0":float(self.c0LineEdit.text()),
                "sigma":float(self.sigmaLineEdit.text()),
                "numOfOut":float(self.numOfOutLineEdit.text())
            }
            """
            coefs = Coefs(
                l=float(self.lLineEdit.text()),
                R= float(self.RLineEdit.text()),
                Toc=float(self.TocLineEdit.text()),
                F0=float(self.F0LineEdit.text()),
                h=float(self.hLineEdit.text()),
                a0=float(self.a0LineEdit.text()),
                aN=float(self.aNLineEdit.text()),
                k0=float(self.k0LineEdit.text()),
                Theta=float(self.ThetaLineEdit.text()),
                T=float(self.TLineEdit.text()),
                m=int(self.mLineEdit.text()),
                p=int(self.pLineEdit.text()),
                eps=float(self.epsLineEdit.text()),
                tau=float(self.tauLineEdit.text()),
                c0=float(self.c0LineEdit.text()),
                sigma=float(self.sigmaLineEdit.text()),
                numOfOut=float(self.numOfOutLineEdit.text())
            )
            """
            self.__system_solver.coefs = coefs
            times, xs, temprs = self.__system_solver.solve_system()
            self.setData(times, xs, temprs)
#            self.__fill_data_for_plots(result)
        except Exception as error:
            QtGui.QMessageBox.warning(self, "Lab4",
                                        str(error), QtGui.QMessageBox.Ok)






