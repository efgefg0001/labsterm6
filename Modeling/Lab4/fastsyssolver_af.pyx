from itertools import islice
from libc.math cimport exp as c_exp
from libc.math cimport pow as c_pow

import numpy as np
cimport numpy as np
cimport cython

DTYPE = np.double
ctypedef np.double_t DTYPE_t


cdef struct Coefs:
   double l
   double R
   double Toc
   double F0
   double h
   double a0
   double aN
   double k0
   double Theta
   double c0
   double T
   int p
   int m
   double tau
   double eps
   double sigma
   int numOfOut


cdef double func(double t, double x):
    return np.sin(x*t)


cdef double comp_a_a(Coefs coefs):
    return coefs.aN * coefs.l / (coefs.aN - coefs.a0)

cdef double comp_F0(double t, Coefs coefs):
    return coefs.F0 #* t * c_exp(-t/coefs.T)

#coefs.__a_k = lambda : -coefs.k0* coefs.kN*coefs.l / (coefs.kN - coefs.k0)


#cdef double b(Coefs coefs):
#    return coefs.kN * coefs.l / (coefs.kN - coefs.k0)


cdef double comp_c(Coefs coefs):
    return - coefs.a0 * comp_a_a(coefs)


cdef double comp_alpha(double x, double a_a, double c):
    return c / (x - a_a)


cdef double comp_p(double x, double alpha, Coefs coefs):
    return 2 / coefs.R * alpha


cdef double comp_f(double x, double alpha, Coefs coefs):
    return 2 / coefs.R * alpha * coefs.Toc


cdef double comp_k(double T, Coefs coefs):
    return coefs.k0# * c_pow(T/coefs.Theta, coefs.m)


cdef double comp_c_t(double T, Coefs coefs):
    return coefs.c0 #* c_pow(T / coefs.Theta, coefs.p)


cdef double comp_kappa(double k_cur, double k_next):
    return 2 * k_cur * k_next / (k_cur + k_next)


cdef double comp_A(double k_prev, double k_cur, Coefs coefs):
#    return comp_kappa(k_prev, k_cur)
    return coefs.sigma * comp_kappa(k_prev, k_cur) / coefs.h * coefs.tau


cdef double comp_C(double k_cur, double k_next, Coefs coefs):
#    return comp_kappa(k_cur, k_next)
    return coefs.sigma * comp_kappa(k_cur, k_next) / coefs.h * coefs.tau


cdef double comp_B(double A, double C, double c_t_next, Coefs coefs):
#    return A + C + p_cur*coefs.h*coefs.h
    return A + C + c_t_next * coefs.h


cdef double comp_F(double kappa_prev, double kappa_next, double c_t_next,
                   double y_x_prev, double y_x_cur, double y_x_next,
                   double f_cur, double p_cur, Coefs coefs):
#    return f_cur * coefs.h* coefs.h
    return c_t_next * y_x_cur * coefs.h + (1-coefs.sigma) * (kappa_prev * (y_x_prev - y_x_cur)/coefs.h - kappa_next * \
        (y_x_cur - y_x_next)/coefs.h) * coefs.tau + p_cur*y_x_cur * coefs.tau * coefs.h - f_cur * coefs.h * coefs.tau


cdef double comp_ksi_next(double A, double B, double C, double ksi_cur):
    return C / (B - A * ksi_cur)


cdef double comp_eta_next(double A, double B, double F, double ksi_cur, double eta_cur):
    return (F + A * eta_cur) / (B - A * ksi_cur)


cdef double comp_K0(double kappa, double c0, double c1, Coefs coefs):
#    return kappa + coefs.h**2 / 8 *(p0 + p1) / 2 + coefs.h* coefs.h / 4 * p0
    return coefs.h / 2 * ((c0 + c1)/4 + c0) + kappa / coefs.h * coefs.tau


cdef double comp_M0(double kappa, double c0, double c1, Coefs coefs):
#    return coefs.h/8 * (p0 + p1) / 2 - kappa
    return coefs.h / 2 * (c0 + c1) / 4 - kappa / coefs.h * coefs.tau


cdef double comp_P0(double F0_t_next,
                    double p0, double p1,
                    double c0, double c1,
                    double f0, double f1,
                    double y0, double y1,
                    Coefs coefs):
#    return coefs.h * coefs.F0 + coefs.h ** 2 / 4 *((f0+f1)/2 + f0)
    return F0_t_next * coefs.tau - coefs.tau*coefs.h/4 * (p0 * y0 - f0 + (p0 + p1)/2 * (y0 + y1)/2 - (f0 + f1)/2) + \
        coefs.h / 2 *((c0 + c1)/2 * (y0 + y1)/2 + c0 * y0)


cdef double comp_ksi_1(double K0, double M0):
    return - M0 / K0


cdef double comp_eta_1(double K0, double P0):
    return P0 / K0


cdef double comp_KN(double kappa, double c_xprev, double c_xlast, Coefs coefs):
#    return kappa - coefs.h * coefs.h / 16 * (p_N_1 + p_N)
    return coefs.h / 2 * (c_xprev + c_xlast) / 4 - kappa / coefs.h * coefs.tau


cdef double comp_MN(double alpha, double kappa, double c_tnext_xlast,
                    double c_tcur_xprev, double c_tcur_xlast, Coefs coefs):
#    return -coefs.h * alpha - kappa - coefs.h* coefs.h/4 * p_N - coefs.h*coefs.h/16 * (p_N_1 + p_N)
    return coefs.h / 2 * (c_tnext_xlast + (c_tcur_xprev + c_tcur_xlast)/4) + kappa / coefs.h * coefs.tau + \
        alpha * coefs.tau


cdef double comp_PN(double alpha, double c_tnext_xlast, double c_tcur_xprev, double c_tcur_xlast,
                    double y_xprev, double y_xlast, double p_xprev, double p_xlast,
                    double f_xprev, double f_xlast, Coefs coefs):
#    return -coefs.h*coefs.h/4 *(f_N + (f_N_1 + f_N)/2) - coefs.h * alpha * coefs.Toc
    return alpha * coefs.Toc * coefs.tau + coefs.h / 2 * (c_tnext_xlast * y_xlast + (c_tcur_xprev + c_tcur_xlast)/2 * \
        (y_xprev + y_xlast)/2) - coefs.tau * coefs.h / 4 * ((p_xprev + p_xlast)/2 * (y_xprev + y_xlast)/2 - (f_xprev + f_xlast)/2 + \
        p_xlast * y_xlast - f_xlast)


cdef double comp_yN(double KN, double MN, double PN, double ksiN, double etaN):
    return (PN - KN * etaN) / (KN*ksiN + MN)


cdef double comp_y_n_1(double ksi_n, double eta_n, double y_n):
    return ksi_n * y_n + eta_n


cdef get_ksi_eta(double t, xs, cur_temprs, Coefs coefs):
#    coefs.a_a, coefs.a_k, coefs.b, coefs.c  = coefs.__a_a(), coefs.__a_k(), coefs.__b(), coefs.__c()
    cdef double a_a = comp_a_a(coefs)
    cdef double c = comp_c(coefs)
    cdef double x = xs[0]
    cdef double k1 = comp_k(0, coefs)
    cdef double kappa = comp_kappa(coefs.k0, k1)
    cdef double p0 = comp_p(x, coefs.a0, coefs)
    cdef double alpha = comp_alpha(x + coefs.h, a_a, c)
    cdef double p1 = comp_p(x + coefs.h, alpha, coefs)
    cdef double c0 = comp_c_t(0, coefs)
    cdef double c1 = comp_c_t(0, coefs)
    cdef double K0 = comp_K0(kappa, c0, c1, coefs)
    cdef double M0 = comp_M0(kappa, c0, c1, coefs)
    cdef double f0 = comp_f(x, coefs.a0, coefs)
    cdef double f1 = comp_f(x+coefs.h, alpha, coefs)
    cdef double P0 = comp_P0(
        comp_F0(t + coefs.tau, coefs),
        p0, p1,
        c0, c1,
        f0, f1,
        cur_temprs[0], cur_temprs[1],
        coefs
    )
    ksi =[
        comp_ksi_1(K0, M0)
    ]
    eta = [
        comp_eta_1(K0, P0)
    ]
    cdef double k_prev = coefs.k0
    cdef double k_cur = k1
    cdef double k_next = comp_k(0, coefs)
    cdef int max_ind = len(xs) - 1
    cdef int i = 1
    for x in islice(xs, 1, max_ind):
        alpha = comp_alpha(x, a_a, c)
        A = comp_A(k_prev, k_cur, coefs)
        C = comp_C(k_cur, k_next, coefs)
        c_t_next = comp_c_t(0, coefs)
        B = comp_B(A, C, c_t_next, coefs)
        F = comp_F(
            kappa_prev=comp_kappa(k_prev, k_cur),
            kappa_next=comp_kappa(k_cur, k_next),
            c_t_next=comp_c_t(0, coefs),
            y_x_prev=cur_temprs[i-1], y_x_cur=cur_temprs[i], y_x_next=cur_temprs[i+1],
            f_cur=comp_f(x, alpha, coefs),
            p_cur=comp_p(x, alpha, coefs),
            coefs=coefs
        )
        ksi.append(comp_ksi_next(A, B, C, ksi[-1]))
        eta.append(comp_eta_next(A, B, F, ksi[-2], eta[-1]))
        k_next = comp_k(0, coefs)
        k_cur = k_next
        k_prev = k_cur
        i += 1
    return (ksi, eta)


cdef compute_Temprs(xs, ksi, eta, cur_temprs, Coefs coefs):
    cdef double k_N_1 = comp_k(0, coefs)
    cdef double k_N = comp_k(0, coefs)
    cdef double a_a = comp_a_a(coefs)
    cdef double c = comp_c(coefs)
    cdef double alpha_N = comp_alpha(xs[-1], a_a, c)
    cdef double alpha_N_1 =  comp_alpha(xs[-2], a_a, c)
    cdef double p_N_1 = comp_p(xs[-2], alpha_N_1, coefs)
    cdef double p_N = comp_p(xs[-1], alpha_N, coefs)
    cdef double f_N_1 = comp_f(xs[-2], alpha_N_1, coefs)
    cdef double f_N = comp_f(xs[-1], alpha_N, coefs)
    cdef double kappa_prev = comp_kappa(k_N_1, k_N)
    cdef double c_xprev = comp_c_t(coefs.Toc, coefs)
    cdef double c_xlast = comp_c_t(coefs.Toc, coefs)
    cdef double KN = comp_KN(
        kappa=kappa_prev,
        c_xprev=c_xprev, c_xlast=c_xlast,
        coefs=coefs
    )
    cdef double c_tnext_xlast = comp_c_t(coefs.Toc, coefs)
    cdef double MN = comp_MN(
        alpha=alpha_N, kappa=kappa_prev,
        c_tnext_xlast=c_tnext_xlast, c_tcur_xprev=c_xprev, c_tcur_xlast=c_xlast,
        coefs=coefs
    )
    cdef double PN = comp_PN(
        alpha=alpha_N,
        c_tnext_xlast=c_tnext_xlast, c_tcur_xprev=c_xprev, c_tcur_xlast=c_xlast,
        y_xprev=cur_temprs[-2], y_xlast=cur_temprs[-1],
        p_xprev=p_N_1, p_xlast=p_N,
        f_xprev=f_N_1, f_xlast=f_N,
        coefs=coefs
    )
    T = [comp_yN(KN, MN, PN, ksi[-1], eta[-1])]
    cdef int max_ind = len(ksi)-1
    cdef int ind = max_ind
    while ind >= 0:
        y_n_1 = comp_y_n_1(ksi[ind], eta[ind], T[0])
        T.insert(0, y_n_1)
        ind -= 1
    return T


@cython.boundscheck(False)
cdef get_tempr_for_time(double t, xs, cur_temprs, coefs):
#    cdef double x
#    temprs = []
#    for x in xs:
#        temprs.append(func(t, x))
    ksi_list, eta_list = get_ksi_eta(t=t, xs=xs, cur_temprs=cur_temprs, coefs=coefs)
#    print("len(ksi) = {}, len(eta) = {}".format(len(ksi_list), len(eta_list)))
    temprs = compute_Temprs(
        xs=xs, ksi=ksi_list, eta=eta_list, cur_temprs=cur_temprs, coefs=coefs
    )
#    print("len(temprs) = {}".format(len(temprs)))
    return temprs


cdef range(double min, double max, double step):
    cdef double item = min
    res_list = []
    cdef double max_item = max + step
    while(item < max_item):
        res_list.append(item)
        item += step
    return res_list

cdef get_start_temprs(double Toc, xs):
    start_temprs = []
    cdef double x
    for x in xs:
        start_temprs.append(Toc)
    return start_temprs


@cython.boundscheck(False)
cpdef solve_system(Coefs coefs):
#        cdef int numOfOut = coefs.numOfOut
#        cdef np.ndarray[DTYPE_t, ndim=1] times = np.arange(0, coefs.T, coefs.tau)
        times = range(0, coefs.T, coefs.tau)
        times_out = []
        xs = range(0, coefs.l, coefs.h)
#        cdef np.ndarray[DTYPE_t, ndim=1] xs = np.arange(0, coefs.l, coefs.h)
#        temprs = []
        prev_temprs = None
        temprs_out = []
        cdef int i = 0
        cdef double t
        cdef max_ind_times = len(times) - 1
        cur_temprs = get_start_temprs(coefs.Toc, xs)
        for t in islice(times, 0, max_ind_times):
            next_temprs = get_tempr_for_time(t, xs, cur_temprs, coefs)
#            print("next_temprs = {}\nlen(next_temprs)={}\nlen(xs)={}".format(next_temprs, len(next_temprs), len(xs)))
            print("next_temprs = {}".format(next_temprs, len(next_temprs), len(xs)))
#            print("len(temprs_for_fixed_t) = {}".format(len(temprs_for_fixed_t)))
#            temprs.append(temprs_for_fixed_t)
            if i % coefs.numOfOut == 0:
                times_out.append(t)
                temprs_out.append(cur_temprs)
            cur_temprs = next_temprs
            i += 1
        try:
            if i % coefs.numOfOut == 0:
                times_out.append(times[i])
                temprs_out.append(cur_temprs)
        except:
            pass
        return times_out, xs, temprs_out