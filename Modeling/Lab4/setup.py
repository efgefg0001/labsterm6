#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from distutils.core import setup
from Cython.Build import cythonize

setup(
  name = 'Tridiagonal matrix system solver',
  ext_modules = cythonize("fastsyssolver.pyx"),
)
