#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
#from PyQt5 import QtWidgets
from PyQt4 import QtGui
from mainwindow import *

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = MainWindow(None)
    window.show()
    sys.exit(app.exec_())
