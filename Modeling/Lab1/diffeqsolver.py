__author__ = 'alex'


import math
import collections

class PicarResult:

    def __init__(self, x, y1, y2, y3):
        self.__x = x
        self.__y1 = y1
        self.__y2 = y2
        self.__y3 = y3

    @property
    def x(self):
        return self.__x

    @property
    def y1(self):
        return self.__y1

    @property
    def y2(self):
        return self.__y2

    @property
    def y3(self):
        return self.__y3


class RungeResult:

    def __init__(self, x, yavnyj, neyavnyj, ya2a1=0, ya2a0_5=0, nya2=0):
        self.__x = x
        self.__yavnyj = yavnyj
        self.__neyavnyj = neyavnyj
        self.ya2a1 = ya2a1
        self.ya2a0_5 = ya2a0_5
        self.nya2 = nya2

    @property
    def x(self):
        return self.__x

    @property
    def yavnyj(self):
        return self.__yavnyj

    @property
    def neyavnyj(self):
        return self.__neyavnyj


class DiffEqSolver:

    def __init__(self):
        self.__f = lambda x, u : x**2 + u**2
        self.__picar_funcs = [
            lambda x : 0,
            lambda x : x **3 / 3,
            lambda x : self.__picar_funcs[1](x) + x ** 7 / 63,
            lambda x : self.__picar_funcs[2](x) + 2 * x ** 11 / 2079 + x ** 15 / 59535
        ]
        self.__runge_funcs = [
            lambda x, h, y_n : y_n + h * (x**2 + y_n**2),
            lambda x, h, y_n : 1/(2*h) - math.sqrt(1 / (4 * h**2) - (y_n + h * x**2)/h),
            lambda x, h, y_n, a=0 : y_n + h*((1-a)*self.__f(x,y_n) + a*self.__f(x + h/(2*a), y_n+h/(2*a)*self.__f(x, y_n) )),
            lambda x, h, y_n, x_n: 1/h-math.sqrt(1/(h**2) - 2/h*(y_n+h/2*(x_n**2 + y_n**2 + x**2)))
        ]

    def solve_Picar(self, x_beg, h_step, max_x):
        res_table = []
        x = x_beg
        #        n = math.ceil(max_x / h_step) + 1
        while x <= max_x:
            res_table.append(PicarResult(x, self.__picar_funcs[1](x),
                                         self.__picar_funcs[2](x),
                                         self.__picar_funcs[3](x)))
            x += h_step
        return res_table

    def solve_Runge(self, x_beg, h_step, max_x):
        res_table = []
        res_table.append(RungeResult(0, 0, 0, 0, 0, 0))
        res = self.__runge_funcs[0](x_beg, h_step, 0)
        i = 1
        x = x_beg
        while x <= max_x:
            x = res_table[i-1].x + h_step
            neyavn, nya2 = 0, 0
            try:
                neyavn = self.__runge_funcs[1](x, h_step, res_table[i-1].neyavnyj)
            except Exception:
                neyavn = "#"
            try:
                nya2 = self.__runge_funcs[3](res_table[i-1].x, h_step, res_table[i-1].nya2, x)
            except Exception:
                nya2 = "#"
            res_table.append(RungeResult(x, self.__runge_funcs[0](res_table[i-1].x, h_step,
                                                                  res_table[i-1].yavnyj),
                                         neyavn,
                                         self.__runge_funcs[2](res_table[i-1].x, h_step, res_table[i-1].ya2a1, 1),
                                         self.__runge_funcs[2](res_table[i-1].x, h_step, res_table[i-1].ya2a0_5, 0.5),
                                         nya2))
            i += 1
        return res_table
