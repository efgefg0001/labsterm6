__author__ = 'alex'

from PyQt5 import QtWidgets, QtGui, QtCore, uic
import random
import math

from diffeqsolver import DiffEqSolver

FormClass = uic.loadUiType('tableview.ui')

class TableViewEx(FormClass[1], FormClass[0]):

    def __init__(self, parent=None):
        FormClass[1].__init__(self, parent)
        self.setupUi(self)
        self.calcButton.clicked.connect(self.handleButton)
        self.__model = self.create_model()
        self.__solver = DiffEqSolver()
        self.resTableView.setModel(self.__model)

    def create_model(self):
        model = QtGui.QStandardItemModel(0, 9, self)
        model.setHorizontalHeaderItem(0, QtGui.QStandardItem("x"))
        model.setHorizontalHeaderItem(1, QtGui.QStandardItem("y(1)"))
        model.setHorizontalHeaderItem(2, QtGui.QStandardItem("y(2)"))
        model.setHorizontalHeaderItem(3, QtGui.QStandardItem("y(3)"))
        model.setHorizontalHeaderItem(4, QtGui.QStandardItem("явная\n1 порядок"))
        model.setHorizontalHeaderItem(5, QtGui.QStandardItem("неявная\n1 порядок"))
        model.setHorizontalHeaderItem(6, QtGui.QStandardItem("явная\n2 порядок\n a=1"))
        model.setHorizontalHeaderItem(7, QtGui.QStandardItem("явная\n2 порядок\n a=0.5"))
        model.setHorizontalHeaderItem(8, QtGui.QStandardItem("неявная\n2 порядок"))
        return model

    def __set_Picar(self, i, row_picar):
        self.__model.setData(self.__model.index(i, 0, QtCore.QModelIndex()), row_picar.x)
        self.__model.setData(self.__model.index(i, 1, QtCore.QModelIndex()), row_picar.y1)
        self.__model.setData(self.__model.index(i, 2, QtCore.QModelIndex()), row_picar.y2)
        self.__model.setData(self.__model.index(i, 3, QtCore.QModelIndex()), row_picar.y3)

    def __set_Runge(self, i, row_runge):
        self.__model.setData(self.__model.index(i, 4, QtCore.QModelIndex()), row_runge.yavnyj)
        self.__model.setData(self.__model.index(i, 5, QtCore.QModelIndex()), row_runge.neyavnyj)
        self.__model.setData(self.__model.index(i, 6, QtCore.QModelIndex()), row_runge.ya2a1)
        self.__model.setData(self.__model.index(i, 7, QtCore.QModelIndex()), row_runge.ya2a0_5)
        self.__model.setData(self.__model.index(i, 8, QtCore.QModelIndex()), row_runge.nya2)

    def __fill_model(self, table_runge, table_picar):
        self.__model.setRowCount(len(table_picar))
        for i in range(self.__model.rowCount()):
            self.__set_Runge(i, table_runge[i])
            self.__set_Picar(i, table_picar[i])

    def handleButton(self):
        try:
            max_x = float(self.xLineEdit.text())
            h_step = float(self.hLineEdit.text())
            table_Runge = self.__solver.solve_Runge(0, h_step, max_x)
            table_Picar = self.__solver.solve_Picar(0, h_step, max_x)
            self.__fill_model(table_Runge, table_Picar)
        except Exception as error:
            QtWidgets.QMessageBox.warning(self, "Lab1",
                                        str(error), QtWidgets.QMessageBox.Ok)
