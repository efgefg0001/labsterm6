#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from interpolator import *
from numpy import arange, pi
from integrator import *
import math,time

import abc

class SystemSolver(metaclass=abc.ABCMeta):

    def __init__(self):
        self.__integr = BadIntegrator()
        self.R = 0.35
        self.I0 = 0
        self.Uc0 = 0
        self.Lk = 0
        self.Ck = 0
        self.Rk = 0
        self.le = 0
        self.t = 0
        self.interp = AllTablesInterpolator()
        self.t_step = 1#10 ** -6
        self.__Tw = 2000
        self.f = lambda I, Uc, Rp : (Uc - (self.Rk + Rp) * I) / self.Lk
        self.phi = lambda I : -I / self.Ck

    def compute_Rp(self, I):
        T = lambda z, T0, n : T0 + (self.__Tw - T0)*(z ** n)
        T0 = self.interp.T0.interpolate(I)
        n =  self.interp.n.interpolate(I)
        ifunc = lambda z : math.fabs(z * self.interp.sigma.interpolate(T(z,T0,n)))

        valT = T(0.4, T0, n)
        sg = self.interp.sigma.interpolate(T(0.4, T0, n))
        val = ifunc(0.4)

        return self.coeff / self.__integr.integrate_lambda(ifunc, 0.0, 1.0)

    def set_all_coefs(self, coefs):
        self.I0 = coefs[0]
        self.Uc0 = coefs[1]
        self.Lk = coefs[2]
        self.Ck = coefs[3]
        self.Rk = coefs[4]
        self.le = coefs[5]
        self.t = coefs[6]


    @property
    def t_step(self):
        return self.__t_step
    
    @t_step.setter
    def t_step(self, value):
        self.__t_step = value

    @property
    def Tw(self):
        return self.__Tw

    @Tw.setter
    def Tw(self, value):
        self.__Tw = value

    @property
    def R(self):
        return self.__R

    @R.setter
    def R(self, value):
        self.__R = value

    @property
    def interp(self):
        return self.__interp
    
    @interp.setter
    def interp(self, value):
        self.__interp = value

    @property
    def I0(self):
        return self.__I0
    
    @I0.setter
    def I0(self, value):
        self.__I0 = value

    @property
    def Uc0(self):
        return self.__Uc0

    @Uc0.setter
    def Uc0(self, value):
        self.__Uc0 = value

    @property
    def Lk(self):
        return self.__Lk

    @Lk.setter
    def Lk(self, value):
        self.__Lk = value
    
    @property
    def Ck(self):
        return self.__Ck
    
    @Ck.setter
    def Ck(self, value):
        self.__Ck = value

    @property
    def Rk(self):
        return self.__Rk

    @Rk.setter
    def Rk(self, value):
        self.__Rk = value
    
    @property
    def le(self):
        return self.__le
    
    @le.setter
    def le(self, value):
        self.__le = value
        self.coeff = self.le/(2 * pi * (self.R ** 2) )

    @property
    def t(self):
        return self.__t

    @t.setter
    def t(self, value):
        self.__t = value

    def test_filling(self, low, high, step):
        res_t = []
        res_i = []
        res_u = []
        res_r = []
        for t in arange(low, high, step):
            res_t.append(t)
            res_i.append(t*t)
            res_u.append(math.exp(t))
            res_r.append(math.sin(t))
        return (res_t, res_i, res_u, res_r)

    @abc.abstractmethod
    def solve_system(self):
        raise NotImplementedError()


class RungeSystemSolver(SystemSolver):

    def __init__(self):
        super().__init__()
        self.__k1 = self.f
        self.__q1 = self.phi
        self.__k2 = lambda I, Uc, Rp, k1, q1 : self.f(I+self.__h_t_step*k1, Uc+self.__h_t_step*q1, Rp)
        self.__q2 = lambda I, k1 : self.phi(I+self.__h_t_step*k1)
        self.__k3 = lambda I, Uc, Rp, k2, q2 : self.f(I+self.__h_t_step*k2, Uc+self.__h_t_step*q2, Rp)
        self.__q3 = lambda I,k2 : self.phi(I+self.__h_t_step*k2)
        self.__k4 = lambda I, Uc, Rp, k3, q3 : self.f(I+self.t_step*k3, Uc+self.t_step*q3, Rp)
        self.__q4 = lambda I,k3 : self.phi(I+self.t_step*k3)
        self.__next = lambda prev, k1, k2, k3, k4 : prev + self.t_step/6*(k1 + 2*k2 + 2*k3 + k4)

    def solve_system(self):
        self.__h_t_step = self.t_step / 2
        t, Ic, Uc, Rp = [], [], [], []
        curr_t = 0
        curr_I = self.I0
        curr_U = self.Uc0
        while curr_t < self.t:
            t.append(curr_t)
            Ic.append(curr_I)
            Uc.append(curr_U)
#            beg = time.process_time()
            curr_Rp = self.compute_Rp(curr_I #/ 10 ** -6
            )
#            duration = time.process_time() - beg
#            print("duration = " + str(duration))
            Rp.append(curr_Rp)

            k1 = self.__k1(curr_I, curr_U, curr_Rp)
            q1 =  self.__q1(curr_I)
            k2 = self.__k2(curr_I, curr_U, curr_Rp, k1, q1)
            q2 = self.__q2(curr_I, k1)
            k3 = self.__k3(curr_I, curr_U, curr_Rp, k2, q2)
            q3 = self.__q3(curr_I, k2)
            k4 = self.__k4(curr_I, curr_U, curr_Rp, k3, q3)
            q4 = self.__q4(curr_I, k3)

            curr_I = self.__next(curr_I, k1, k2, k3, k4)
            curr_U = self.__next(curr_U, q1, q2, q3, q4)
            curr_t += self.t_step

        return (t, Ic, Uc, Rp)


class ImplSystemSolver(SystemSolver):

    def __init__(self):
        super().__init__()
        self.__eps = 10**-4
        self.__next_u = lambda u_cur, i_cur, i_next : u_cur - self.t_step/(2*self.Lk)*(i_cur+i_next)
        self.__next_i = lambda u_cur, i_cur, r_cur, r_s : \
            (self.t_step/self.Lk * u_cur + (1-0.25*self.t_step**2/(self.Lk * self.Ck) - 0.5*self.t_step/self.Lk*(self.Rk + r_cur))*i_cur)/ \
            (0.25/(self.Lk*self.Ck) + 0.5*self.t_step/self.Lk*(self.Rk + r_s)+1)
        # i_s_n - i with s + 1 iteration
        # i_s - i with s iteration
        self.__end = lambda i_s_n, i_s : abs((i_s_n - i_s)/i_s_n) < self.__eps

    def solve_system(self):
        self.__h_t_step = self.t_step / 2
        t, Ic, Uc, Rp = [], [], [], []
        curr_t = 0
        curr_I = self.I0
        curr_U = self.Uc0
        while curr_t < self.t:
            t.append(curr_t)
            Ic.append(curr_I)
            Uc.append(curr_U)
            curr_Rp = self.compute_Rp(curr_I)
            Rp.append(curr_Rp)
            I_s = curr_I
            I_s_n = self.__next_i(curr_U, curr_I, curr_Rp, curr_Rp)
            while not self.__end(I_s_n, I_s):
                I_s = I_s_n
                Rp_s = self.compute_Rp(I_s)
                I_s_n = self.__next_i(curr_U, curr_I, curr_Rp, Rp_s)
            curr_t += self.t_step
            curr_U = self.__next_u(curr_U, curr_I, I_s_n)
            curr_I = I_s_n
        print("end")
        return (t, Ic, Uc, Rp)



