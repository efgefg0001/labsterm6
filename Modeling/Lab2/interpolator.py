#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import abc
from scipy.interpolate import *
from numpy import arange
from sampling import *
import time

class Interpolator(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def interpolate(self, value):
        raise NotImplementedError()


class LInterpolator(Interpolator):

    def __init__(self, list_x, list_y):
        self.__list_x = list_x
        self.__list_y = list_y
        self.__interp_f = InterpolatedUnivariateSpline(list_x, list_y, k=1)#lagrange(list_x, list_y)

    def interpolate(self, value):
        result = 0
        try:
            result = self.__interp_f(value)
        except Exception as error:
            print("value = " + str(value) + " res = " + str(result))
        return result


class AllTablesInterpolator:

    def __init__(self):
        self.list_I = [0.5, 1., 5., 10., 50., 200., 400., 800., 1200.]
        self.list_T0 = [6400., 6790., 7150., 7270., 8010., 9185., 10010., 11140., 12010.]
        self.list_n = [0.4, 0.55, 1.7, 3., 11., 32., 40., 41., 39.]
        self.list_T = arange(4000.0, 14001, 1000.0)
        self.list_sigma = [0.031, 0.27, 2.05, 6.06, 12.0, 19.9, 29.6, 41.1, 54.1, 67.7, 81.5]
        self.T0 = LInterpolator(self.list_I, self.list_T0)
        self.n = LInterpolator(self.list_I, self.list_n)
        self.sigma = LInterpolator(self.list_T, self.list_sigma)
