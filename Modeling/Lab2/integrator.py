#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import abc

from scipy.integrate import quad
from scipy.interpolate import lagrange
import time

class Integrator(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def integrate_lambda(self, func, low, high):
        raise NotImplementedError()

    @abc.abstractmethod
    def integrate_values(self, list_x, list_y):
        raise NotImplementedError()

class BadIntegrator(Integrator):

    def __init__(self):
        self.num = 10

    def integrate_lambda(self, func, low, high):
#        beg = time.process_time()
        step = (high - low) / self.num
        sum = (func(low) + func(high))/2
        x = low
        for i in range(1, self.num):
            f = func(x)
            sum += f
            x += step
        res = sum*step
#        print("dur = " + str(time.process_time() - beg))
        return res

    def integrate_values(self, list_x, list_y):
        lagr = lagrange(list_x, list_y)
        return self.integrate_lambda(lagr, min(list_x), max(list_x))

class LIntegrator(Integrator):

    def __init__(self):
        self.__integ_func = quad

    def integrate_lambda(self, func, low, high):
        result = self.__integ_func(func, low, high)[0]
        return result

    def integrate_values(self, list_x, list_y):
        lagr_pol = lagrange(list_x, list_y)
        return self.__integ_func(lagr_pol, min(list_x), max(list_y))[0]
