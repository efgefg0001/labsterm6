#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib
matplotlib.use("Qt5Agg")
from PyQt5 import QtWidgets, QtGui, QtCore, uic
from PyQt5.QtWidgets import QHBoxLayout, QSizePolicy, QMessageBox, QWidget
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

from systemsolver import *

FormClass = uic.loadUiType('mainwindow.ui')

class MainWindow(FormClass[1], FormClass[0]):

    def __init__(self, parent=None):
        FormClass[1].__init__(self, parent)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setupUi(self)
        self.verticalLayout.addLayout(self.__create_plots())
        self.solveButton.clicked.connect(self.onClickedSolveButton)

        self.__runge = RungeSystemSolver()
        self.__impl = ImplSystemSolver()

    def __create_plots(self):
        self.__i_t_plot = MplCanvas("t", "Ic")
        self.__u_t_plot = MplCanvas("t", "Uc")
        self.__r_t_plot = MplCanvas("t", "Rp")
        horiz = QHBoxLayout()
        horiz.addWidget(self.__i_t_plot)
        horiz.addWidget(self.__u_t_plot)
        horiz.addWidget(self.__r_t_plot)
        return horiz

    def __fill_data_for_plots(self, data):
        self.__i_t_plot.load_data(data[0], data[1])
        self.__u_t_plot.load_data(data[0], data[2])
        self.__r_t_plot.load_data(data[0], data[3])
    def clear_all_plots(self):
        self.__i_t_plot.clear_plot()
        self.__u_t_plot.clear_plot()
        self.__r_t_plot.clear_plot()

    def onClickedSolveButton(self):
        self.clear_all_plots()
        try:
            coefs = (
                float(self.iLineEdit.text()),
                float(self.uLineEdit.text()),
                float(self.lkLineEdit.text()), #* 10 ** -6,
                float(self.ckLineEdit.text()), #* #10 ** -6,
                float(self.rkLineEdit.text()),
                float(self.leLineEdit.text()),
                float(self.tLineEdit.text()), #* 10 ** -6
            )
            result = ()
            if self.runkeRadioButton.isChecked():
                self.__runge.set_all_coefs(coefs)
                result = self.__runge.solve_system()
            elif self.implRadioButton.isChecked():
                self.__impl.set_all_coefs(coefs)
                result = self.__impl.solve_system()
            self.__fill_data_for_plots(result)
        except Exception as error:
            QtWidgets.QMessageBox.warning(self, "Lab2",
                                        str(error), QtWidgets.QMessageBox.Ok)



class MplCanvas(FigureCanvas):

    def __init__(self, xlabel, ylabel, parent=None, width=5, height=4, dpi=70):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        # We want the axes cleared every time plot() is called
        self.axes.hold(True)
#        self.draw_plot()
        FigureCanvas.__init__(self, fig)
        self.setParent(parent)
        FigureCanvas.setSizePolicy(self,
                QSizePolicy.Expanding,
                QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        self.__xlabel = xlabel
        self.__ylabel = ylabel
        self.axes.set_xlabel(xlabel)
        self.axes.set_ylabel(ylabel, rotation=0)
        self.__color_f = "r"

    def load_data(self, x, y):
        self.__x = x
        self.__y = y
        self.draw_plot()

    def clear_plot(self):
        self.axes.cla()
        self.draw()

    def draw_plot(self):
        self.axes.set_xlabel(self.__xlabel)
        self.axes.set_ylabel(self.__ylabel, rotation=0)
        self.axes.plot(self.__x, self.__y, self.__color_f)
        self.draw()

