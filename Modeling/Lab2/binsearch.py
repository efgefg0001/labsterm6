#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def bi_search(lst, value, compare):
    low = 0
    high = len(lst) - 1
    result = -1
    while low <= high and result == -1:
        mid = (low + high)//2
        res_of_comp = compare(lst[mid], value)
        if res_of_comp == 0:
            result = mid
        elif res_of_comp > 0:
            high = mid - 1
        else:
            low = mid + 1
    if result == - 1:
        if low >= len(lst)-1:
            result = len(lst) - 1
        elif low <= 0:
            result = 0
        elif compare(lst[low], value) < 0 and compare(lst[low+1], value) > 0:
            result = low
        elif compare(lst[low], value) > 0 and compare(lst[low-1], value) < 0:
            result = low-1;
    return result
