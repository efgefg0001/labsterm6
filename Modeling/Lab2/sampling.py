#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import abc

from binsearch import *

class Sampler():

    @abc.abstractmethod
    def sample_near_val(self, table, value, n):
        raise NotImplementedError()

    @abc.abstractmethod
    def sample_near_index(self, table, index, n):
        raise NotImplementedError()


class NewtonInterpSampler(Sampler):

    def __init__(self, find=bi_search):
        self.__find = find
        self.__compare = \
            lambda x, y : 0 if x[0] == y else -1 \
                if x[0] < y else 1

    def sample_near_index(self, table, index, n):
        table_len = len(table)
        index_list = []
        if n >= table_len:
            index_list = [i for i in range(table_len)]
        else:
            half = table_len // 2
            low = index - half + 1
            high = low + table_len
        return index_list

    def sample_near_val(self, table, value, n):
        index = self.__find(self.__table, value, n)
        return self.sample_near_index(table, index, n)
