#ifndef PTYFORK_H
#define PTYFORK_H
#include <termios.h>
#include <sys/ioctl.h>

pid_t pty_fork(int *ptrfdm, char *slave_name, int slave_namesz,
			   const struct termios *slave_termios,
			   const struct winsize *slave_winsize);

#endif // PTYFORK_H
