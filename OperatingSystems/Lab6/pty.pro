TEMPLATE = app
CONFIG -= console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.c \
    ptyfork.c \
    ptyopen.c \
    driver.c \
    error.c \
    loop.c \
    signalintr.c \
    spipe.c \
    ttymodes.c \
    writen.c

HEADERS += \
    driver.h \
    error.h \
    loop.h \
    ptyfork.h \
    ptyopen.h \
    signalintr.h \
    spipe.h \
    ttymodes.h \
    writen.h

QMAKE_CFLAGS += -Wall \
    -Wextra \
    -Wmissing-prototypes \
    -Wstrict-prototypes

DEFINES += "_XOPEN_SOURCE=700"
