#ifndef TTYMODES_H
#define TTYMODES_H
#include <termios.h>

int tty_cbreak(int fd);
int tty_raw(int fd);
int tty_reset(int fd);
void tty_atexit(void);
#ifdef	ECHO
struct termios *tty_termios(void);
#endif

#endif // TTYMODES_H
