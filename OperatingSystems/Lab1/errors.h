#ifndef ERRORS_H
#define ERRORS_H

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <stdarg.h>

#define MAXLINE 4096

void err_quit(const char *, ...);

void err_exit(int error, const char *fmt, ...);


#endif // ERRORS_H
