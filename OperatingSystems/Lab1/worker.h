#ifndef WORKER_H
#define WORKER_H

#include <stdlib.h>
#include <syslog.h>

const int TIMEOUT = 5;
const int REPETITION = 8;

void worker_func()
{
    int i;
    char *line = "============================";
    openlog("Lab1", LOG_PID, LOG_DAEMON);
    syslog(LOG_INFO, line);
    syslog(LOG_INFO, "Начало выполнения демона");
    for (i = 0; i < REPETITION; ++i)
    {
        sleep(TIMEOUT);
        syslog(LOG_INFO, "Прошло %d секунд со старта демона.", TIMEOUT * (i+1));
    }
    syslog(LOG_INFO, "Конец выполнения демона");
    syslog(LOG_INFO, line);
    closelog();
}

#endif // WORKER_H
