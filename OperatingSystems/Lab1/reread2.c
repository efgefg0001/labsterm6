#include <syslog.h>
#include <errno.h>

#include "daemon_utils.h"
#include "in_out.h"
#include "worker.h"

void sigterm(int signo)
{
    syslog(LOG_INFO, "получен сигнал SIGTERM; выход");
	exit(0);
}

void sighup(int signo)
{
    syslog(LOG_INFO, "получен сигнал SIGHUP.");
}

int main(int argc, char *argv[])
{
    char *cmd;
    struct sigaction sa;

	if ((cmd = strrchr(argv[0], '/')) == NULL)
		cmd = argv[0];
	else
		cmd++;

	/*
     * Перейти в режим демона.
	 */
	daemonize(cmd);

	/*
     * Убедиться в том, что ранее не была запущена другая копия демона.
	 */
	if (already_running()) {
        syslog(LOG_ERR, "демон уже запущен");
		exit(1);
	}

	/*
     * Установить обработчики сигналов.
     */
	sa.sa_handler = sigterm;
	sigemptyset(&sa.sa_mask);
	sigaddset(&sa.sa_mask, SIGHUP);
	sa.sa_flags = 0;
	if (sigaction(SIGTERM, &sa, NULL) < 0) {
        syslog(LOG_ERR, "невозможно перехватить сигнал SIGTERM: %s", strerror(errno));
		exit(1);
	}
	sa.sa_handler = sighup;
	sigemptyset(&sa.sa_mask);
	sigaddset(&sa.sa_mask, SIGTERM);
	sa.sa_flags = 0;
	if (sigaction(SIGHUP, &sa, NULL) < 0) {
        syslog(LOG_ERR, "невозможно перехватить сигнал SIGHUP: %s", strerror(errno));
		exit(1);
    }
	/*
     * Остальная часть программы-демона.
	 */
    worker_func();
	exit(0);
}
