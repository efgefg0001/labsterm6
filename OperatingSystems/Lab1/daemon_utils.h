#ifndef DAEMON_UTILS_H
#define DAEMON_UTILS_H

#include <syslog.h>
#include <fcntl.h>
#include <sys/resource.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <stdlib.h>

#include "errors.h"

#define LOCKFILE "/var/run/Lab1.pid"
#define LOCKMODE (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)

void daemonize(const char *cmd);

int lockfile(int);

int already_running(void);

#endif // DAEMON_UTILS_H
