TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    daemon_utils.c \
    errors.c \
    reread2.c

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    daemon_utils.h \
    errors.h \
    worker.h

QMAKE_CXXFLAGS += -std=c++0x -pthread
LIBS += -pthread
