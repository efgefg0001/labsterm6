#include<stdio.h> //printf
#include<string.h>    //strlen
#include<sys/socket.h>    //socket
#include<arpa/inet.h> //inet_addr
 
int main(int argc , char *argv[])
{
    int sock;
    int i=0;
    struct sockaddr_in server;
    char message[1000] , server_reply[2000];
     
    sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock == -1)
    {
        printf("Ошибка при вызове функции socket()");
        return 1;
    }
    puts("Сокет создан");
     
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_family = AF_INET;
    server.sin_port = htons( 8888 );
 
    if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
    {
        perror("Ошибка соединения.");
        return 1;
    }
     
    puts("Соединение установлено\n");
     
    while(1)
    {
        printf("Введите сообщение: ");
        scanf("%s" , message);
         
        if(write(sock , message , strlen(message)) < 0)
        {
            puts("Ошибка посылки сообщения");
            return 1;
        }
        for(i=0; i < 2000; ++i)         
            server_reply[i] = 0;
        if(read(sock , server_reply , 2000) < 0)
        {
            puts("Ошибка вызова функции recv()");
            break;
        }
         
        puts("Ответ сервера :");
        puts(server_reply);
    }
     
    close(sock);
    return 0;
}
