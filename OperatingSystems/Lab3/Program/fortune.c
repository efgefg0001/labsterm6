#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h> 
#include <linux/string.h>
#include <linux/vmalloc.h>
#include <linux/seq_file.h>
#include <asm/uaccess.h>


MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Fortune Cookie Kernel Module");
MODULE_AUTHOR("Vasyunin A. N.");



#define MAX_COOKIE_LENGTH       PAGE_SIZE
static struct proc_dir_entry *proc_entry;


static char *cookie_pot;  // Хранилище 'фортунок'
static int cookie_index;  // Индекс первого свободного для записи элемента хранилища
static int next_fortune;  // Индекс элемента хранилища, содержащего 
                                      // следующую 'фортунку' для вывода по запросу

ssize_t fortune_write( struct file *filp, const char __user *buff,
                    unsigned long len, void *data )
{
    int space_available = (MAX_COOKIE_LENGTH-cookie_index)+1;
    if (len > space_available) {
        printk(KERN_INFO "fortune: cookie pot is full!\n");
        return -ENOSPC;
    }
    if (copy_from_user( &cookie_pot[cookie_index], buff, len )) {
        return -EFAULT;
    }
    cookie_index += len;
    cookie_pot[cookie_index-1] = 0;
    printk(KERN_INFO, "fortune: Write");
    return len;
}


int fortune_read( char *page, char **start, off_t off,
                int count, int *eof, void *data )
{
    int len;
    if (off > 0) {
        *eof = 1;
        return 0;
    }
    /* Перевод индекса на первый элемент */
    if (next_fortune >= cookie_index) 
        next_fortune = 0;
    len = sprintf(page, "%s\n", &cookie_pot[next_fortune]);
    next_fortune += len;
    printk(KERN_INFO, "fortune: Read");
    return len;
}


static const struct file_operations fort_proc_fops = {
    .owner = THIS_MODULE,
    .read = fortune_read,
    .write = fortune_write,
    .llseek = seq_lseek,
    .release = single_release,
};


static int __init fortune_init( void )
{
    int ret = 0;
    cookie_pot = (char *)vmalloc( MAX_COOKIE_LENGTH );
    if (!cookie_pot) {
        ret = -ENOMEM;
    } else {
        memset( cookie_pot, 0, MAX_COOKIE_LENGTH );
        proc_entry = proc_create( "fortune", 0644, NULL, &fort_proc_fops);
        if (proc_entry == NULL) {
            ret = -ENOMEM;
            vfree(cookie_pot);
            printk(KERN_INFO "fortune: Couldn't create proc entry\n");
        } else {
            cookie_index = 0;
            next_fortune = 0;
            printk(KERN_INFO "fortune: Module loaded.\n");
        }
    }
    return ret;
}


static void __exit fortune_exit( void )
{
    remove_proc_entry("fortune", proc_entry);
    vfree(cookie_pot);
    printk(KERN_INFO "fortune: Module unloaded.\n");
}


module_init( fortune_init );
module_exit( fortune_exit );
