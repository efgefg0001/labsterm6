#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'alex'


import abc


# Реализация
class DrawingApi(metaclass=abc.ABCMeta):

    abc.abstractmethod
    def drawCircle(self, x, y, radius):
        raise NotImplementedError()


# Конкретная реализация 1
class DrawingApiFirst(DrawingApi):
    def drawCircle(self, x, y, radius):
            print("ApiFirst.circle at %f:%f radius %f" % (x, y, radius))


# Конкретная реализация 2
class DrawingApiSecond(DrawingApi):
    def drawCircle(self, x, y, radius):
            print("ApiSecond.circle at %f:%f radius %f" % (x, y, radius))


# Абстракция
class Shape(metaclass=abc.ABCMeta):

    def __init__(self, drawingApi):
        self.drawingApi = drawingApi

    def getDrawingApi(self):
        return self.drawingApi

    abc.abstractmethod
    def draw(self):
        raise NotImplementedError()

    abc.abstractmethod
    def resizeByPercentage(self, pct):
        raise NotImplementedError()


# Уточненная абстракция
class CircleShape(Shape):
    def __init__(self, x, y, radius, drawingApi):
        super().__init__(drawingApi)
        self.__x = x
        self.__y = y
        self.__radius = radius

    def draw(self):
        self.getDrawingApi().drawCircle(self.__x, self.__y, self.__radius)

    def resizeByPercentage(self, pct):
        self.__radius *= pct


def main():
    shapes = [
        CircleShape(1, 2, 3, DrawingApiFirst()),
        CircleShape(5, 7, 11, DrawingApiSecond())
    ]

    for shape in shapes:
        shape.resizeByPercentage(2.5)
        shape.draw()
        print("-" * 20)

if __name__ == "__main__":
    main()
