#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'alex'

import abc
import random


class Game(metaclass=abc.ABCMeta):

    def __init__(self):
        self.players_count = 0
        self.end_num = 10

    abc.abstractmethod
    def end_of_game(self):
        raise NotImplementedError()

    abc.abstractmethod
    def initialize_game(self):
        raise  NotImplementedError()

    abc.abstractmethod
    def make_play(self, player):
        raise NotImplementedError()

    abc.abstractmethod
    def print_winner(self):
        raise  NotImplementedError()

    def play_one_game(self, players_count):
        self.players_count = players_count
        self.initialize_game()
        j = 0
        while not self.end_of_game():
            self.make_play(j)
            j = (j + 1) % players_count
        self.print_winner()


class Monopoly(Game):

    def initialize_game(self):
        print("Monopoly: initialization")

    def make_play(self, player):
        print("Monopoly: player's #{0} turn".format(player))

    def end_of_game(self):
        a = random.randint(0, self.end_num)
        b = random.randint(0, self.end_num)
        return a == b

    def print_winner(self):
        print("Monopoly: player #{0} won".format(random.randrange(0, self.players_count)))


class Darts(Game):

    def initialize_game(self):
        print("Darts: initialization")

    def make_play(self, player):
        print("Darts: player's #{0} turn".format(player))

    def end_of_game(self):
        a = random.randint(0, self.end_num)
        b = random.randint(0, self.end_num)
        return a - 1 == b + 1

    def print_winner(self):
        winner = abs(random.randrange(-self.players_count, self.players_count))
        print("Darts: player #{0} won".format(winner))


if __name__ == "__main__":
    #game = Monopoly()
    game = Darts()
    game.play_one_game(4)
