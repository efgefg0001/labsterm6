#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'alex'

# Сложные части системы
class CPU:
    def __init__(self):
        self.name = self.__class__.__name__

    def freeze(self):
        print("{0}: freeze".format(self.name))

    def jump(self, address):
        print("{0}: jump, address = {1}".format(self.name, address))

    def execute(self):
        print("{0}: execute".format(self.name))

class Memory:
    def __init__(self):
        self.name = self.__class__.__name__

    def load(self, position, data):
        print("{0}: load, position = {1}, data = {2}".format(self.name, position, data))


class HardDrive:
    def __init__(self):
        self.name = self.__class__.__name__

    def read(self, lba, size):
        print("{0}: read, lba = {1}, size = {2}".format(self.name, lba, size))
        return "\"some data\""


# Фасад
class Computer:

    # Константы
    BOOT_ADDRESS = 0
    BOOT_SECTOR = 0
    SECTOR_SIZE = 10

    def __init__(self):
        self._cpu = CPU()
        self._memory = Memory()
        self._hardDrive = HardDrive()

    def startComputer(self):
        self._cpu.freeze()
        self._memory.load(self.BOOT_ADDRESS, self._hardDrive.read(self.BOOT_SECTOR, self.SECTOR_SIZE))
        self._cpu.jump(self.BOOT_ADDRESS)
        self._cpu.execute()


# Клиентская часть
if __name__ == "__main__":
    facade = Computer()
    facade.startComputer()
