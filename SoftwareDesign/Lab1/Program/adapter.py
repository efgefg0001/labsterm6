#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'alex'


import abc


class Cable:

    def create_cable_picture(self):
        return 'picture from cable'


class Antenna:

    def create_wave_picture(self):
        return 'picture from antenna'

class PictureCreator(metaclass=abc.ABCMeta):

    abc.abstractmethod
    def get_picture(self):
        raise NotImplementedError()


class SourceCable(PictureCreator):

    def __init__(self):
       self.creator = Cable()

    def get_picture(self):
        return self.creator.create_cable_picture()


class SourceAntenna(PictureCreator):

    def __init__(self):
        self.creator = Antenna()

    def get_picture(self):
        return self.creator.create_wave_picture()


class TV:

    def __init__(self, source):
        self.source = source

    def show_picture(self):
        return 'TV: ' + self.source.get_picture()


if __name__ == '__main__':
    cabel_tv = TV(SourceCable())
    antenna_tv = TV(SourceAntenna())
    print(cabel_tv.show_picture())
    print(antenna_tv.show_picture())
