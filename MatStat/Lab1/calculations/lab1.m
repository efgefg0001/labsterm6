function lab1()
    
    X0 = importdata('data-x.txt');
    X = sort(X0);
    
    % max
    Mmax = max(X);
    fprintf('Mmax = %.4f\n', Mmax);
    
    % min
    Mmin = min(X);
    fprintf('Mmin = %.4f\n', Mmin);
    
    % R
    R = compute_R(X);
    fprintf('R = %.4f\n', R);
    
    % mu
    mu = compute_mu(X);
    fprintf('mu = %.4f\n', mu);

    % disp
    s2 = compute_s2(X);
    fprintf('s2 = %.4f\n', s2);
    
    % delta
    delta = compute_delta(X);
    fprintf('delta = %.4f\n', delta);
    
    % m 
    m = compute_m(X);
    fprintf('m = %.4f\n', m);
    
    % interv
    J = compute_J(X);
    disp(J);
    
    % hist
    % norm
    build_graph_1(J, X);
    build_graph_2(X);
    
    
end

function build_graph_1(intervals, X)
    n = height(intervals);
    nm = compute_delta(X) * length(X);
    res_x = [];
    res_y = [];
    ind_x = 0;
    ind_y = 0;
    for i = 1:n
        ind_x = ind_x + 1; ind_y = ind_y + 1;
        res_x(ind_x) = intervals.intervBeg(i);
        res_y(ind_y) = intervals.numOfValues(i)/nm;
        ind_x = ind_x + 1; ind_y = ind_y + 1;
        res_x(ind_x) = intervals.intervEnd(i);
        res_y(ind_y) = res_y(ind_y-1);
        ind_x = ind_x + 1; ind_y = ind_y + 1;
        res_x(ind_x) = NaN;
        res_y(ind_y) = NaN;
    end
    mu = compute_mu(X);
    s2 = compute_s2(X);
    figure;
    plot(X, normpdf(X,mu,sqrt(s2)), 'r', res_x, res_y, 'b');
end
function build_graph_2(X)
    n = length(X);
    res_x = [];
    res_y = [];
    ind_x = 0;
    ind_y = 0;
    for i = 1:n - 1
        ind_x = ind_x + 1; ind_y = ind_y + 1;
        res_x(ind_x) = X(i);
        res_y(ind_y) = (i+1)/n;
        ind_x = ind_x + 1; ind_y = ind_y + 1;
        res_x(ind_x) = X(i+1);
        res_y(ind_y) = (i+1)/n;
        ind_x = ind_x + 1; ind_y = ind_y + 1;
        res_x(ind_x) = NaN;
        res_y(ind_y) = NaN;
    end
    mu = compute_mu(X);
    s2 = compute_s2(X);
    figure;
    plot(X, normcdf(X,mu,sqrt(s2)), 'r', res_x, res_y, 'b');
end

function R = compute_R(X)
    R = max(X) - min(X);
end

function mu = compute_mu(X)
    n = length(X);
    mu = 1/n * sum(X);
end

function s2 = compute_s2(X)
    mu = compute_mu(X);
    n = length(X);
    s2 = 1/(n-1) * sum((X - mu).^2);
end

function m = compute_m(X)
    n = length(X);
    m = floor(log2(n)) + 2;
end

function delta = compute_delta(X)
    m = compute_m(X);
    R = compute_R(X);
    delta = R / m;
end

function res = contains(x, intervBeg, intervEnd)
    n = length(intervBeg);
    res = -1;
    for i = 1:n
        if (i == n)
            if(intervBeg(i) <= x) && (x <= intervEnd(i))
                res = i;
            end
        else
            if(intervBeg(i) <= x) && (x < intervEnd(i))
                res = i;
                break;
            end
        end
    end
end

function J = compute_J(X)
    m = compute_m(X);
    step = compute_R(X)/m;
    Mmin = min(X);
    intervBeg = zeros(m, 1);
    intervEnd = zeros(m,1);
    numOfValues = zeros(m, 1);
    for i = 1:m
        intervBeg(i) = Mmin + (i-1)*step;
        intervEnd(i) = Mmin + i*step;
    end
    for i = 1:length(X)
        j = contains(X(i), intervBeg, intervEnd);
        if not (j == -1)
            numOfValues(j) = numOfValues(j) + 1;
        end
    end
    J = table(intervBeg, intervEnd, numOfValues);
end

function build_hist_for_density(X)
    m = compute_m(X);
    hist(X, m);
end
