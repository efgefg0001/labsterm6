function lab2()
    X0 = importdata('data-x.txt');
    X = X0;

    gamma = 0.9;
    N = length(X);

    % task 1    
    % a)
    fprintf('\nTask 1.\n');
    fprintf('a) Point estimations:\n');
    p_mu = compute_mu(X);
    fprintf('\tMX: mu = %.4f\n', p_mu);
    p_sigma = compute_sigma2(X);
    fprintf('\tDX: sigma^2 = %.4f\n', p_sigma);
    % b)
    fprintf('b) MX limits\n');
    fprintf('DX is known\n');
    mu_g = compute_mu_g_known_sigma2(X, gamma);
    fprintf('\tlow = %.4f high = %.4f\n', mu_g(1), mu_g(2));
    fprintf('DX is unknown\n');
    mu_g_un = compute_mu_g_unknown_sigma2(X, gamma);
    fprintf('\tlow = %.4f high = %.4f\n', mu_g_un(1), mu_g_un(2));

    % c)
    fprintf('c) DX limits\n');
    sigma2_g = compute_sigma2_g(X, gamma);
    fprintf('\tlow = %.4f high = %.4f\n', sigma2_g(1), sigma2_g(2));

    
    % task 2 
    % mu
    fprintf('\nTask 2.\n');
    mu = compute_mu(X);
    fprintf('\tmu = %.4f\n', mu);
    % disp
    s2 = compute_s2(X);
    fprintf('\ts2 = %.4f\n', s2);

    % task 3
    % a)
    build_plot_a(X, gamma, N);
    % b)
    build_plot_b(X, gamma, 120);
end

function build_plot_a(X, gamma, N)
    n = [];
    const_mu = [];
    mu_hat = [];
    mu_un = [];
    mu_ov = [];
    for i=1:N
        Xloc = X(1:i);
        n(i) = i;
        const_mu(i) = compute_mu(X(1:N));
        mu_hat(i) = compute_mu(Xloc);
        mu_g = compute_mu_g_known_sigma2(Xloc, gamma);
        mu_un(i) = mu_g(1);
        mu_ov(i) = mu_g(2);
    end
    figure;
    plot(n, const_mu, n, mu_hat, n, mu_un, n, mu_ov);
    xlabel('n');
    ylabel('y');
    legend({
        'average X for XN',
        'average X for Xn';
        'low limit of MX for Xn';
        'high limit of MX for Xn'
    });
end

function build_plot_b(X, gamma, N)
    n = [];
    const_s2 = [];
    s2 = [];
    sigma2_un = [];
    sigma2_ov = [];
    for i=1:N
        Xloc = X(1:i);
        n(i) = i;
        const_s2(i) = compute_s2(X(1:N));
        s2(i) = compute_s2(Xloc);
        sigma2_g = compute_sigma2_g(Xloc, gamma);
        sigma2_un(i) = sigma2_g(1);
        sigma2_ov(i) = sigma2_g(2);
    end
    figure;
    plot(n(20:N), const_s2(20:N), n(20:N), s2(20:N), n(20:N), sigma2_un(20:N), n(20:N), sigma2_ov(20:N));
    xlabel('n');
    ylabel('z');
    legend({
        'S^2 for XN'
        'S^2 for Xn' ;
        'low limit of DX for Xn';
        'high limit of DX for Xn'
    });
end



function mu = compute_mu(X)
    n = length(X);
    mu = 1/n * sum(X);
end
function sigma2 = compute_sigma2(X)
    mu = compute_mu(X);
    n = length(X);
    sigma2 = 1/n * sum((X - mu).^2);
end

function s2 = compute_s2(X)
    mu = compute_mu(X);
    n = length(X);
    s2 = 1/(n-1) * sum((X - mu).^2);
end

function mu_g = compute_mu_g_known_sigma2(X, gamma)
    sigma = sqrt(compute_sigma2(X));
    av_x = compute_mu(X);
    u = norminv((1+gamma)/2, 0, 1);
    n = length(X);
    mu_g = [
        av_x - sigma/sqrt(n)*u, 
        av_x + sigma/sqrt(n)*u
    ];
end

function mu_g = compute_mu_g_unknown_sigma2(X, gamma)
    s = sqrt(compute_s2(X));    
    av_x = compute_mu(X);
    n = length(X);
    t = tinv((1 + gamma)/2, n-1);
    mu_g = [
        av_x - s/sqrt(n)*t,
        av_x + s/sqrt(n)*t
    ];
    
    
end

function sigma2_g = compute_sigma2_g(X, gamma)
    s2 = compute_s2(X);
    n = length(X);
    h_low = chi2inv((1+gamma)/2, n-1);
    h_high = chi2inv((1-gamma)/2, n-1);
    sigma2_g = [
        s2*(n-1)/h_low,
        s2*(n-1)/h_high
    ];
end



