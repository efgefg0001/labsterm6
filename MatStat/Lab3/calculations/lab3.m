function lab3()
    % y^ = teta1 * 1 + teta2 * t + teta3 * t^2
    t = importdata('t.txt');
    y = importdata('y.txt');
    teta = compute_teta(y, t);
    fprintf('teta = [%f, %f, %f]\n', teta(1), teta(2), teta(3));
    delta = compute_delta(y, t, teta);
    fprintf('delta = %f\n', delta);
    build_plot(y, t, teta);
end
function y_approx = compute_y_approx(teta, t_i)
    y_approx = teta(1) + teta(2) * t_i + teta(3) * t_i ^ 2 + teta(4) * t_i ^ 3;
end
function delta = compute_delta(y, t, teta)
    delta_loc = 0;
    n = length(t);
    for i=1:n
        delta_loc = delta_loc + (y(i) - compute_y_approx(teta, t(i))) ^2;
    end
    delta = sqrt(delta_loc);
end

function build_plot(y, t, teta)
    y_appr = [];
    n = length(t);
    for i=1:n
        y_appr(i) = compute_y_approx(teta, t(i));
    end
    figure;
    plot(t, y, '.b', t, y_appr, 'r');
    xlabel('t');
    ylabel('y');
    legend({
        'точное значение';
        'приближённое значение';
    });
end
function matr = create_matrix(t)
    psi = [];
    n = length(t);
    for i=1:n
        psi(i,1) = 1;
        psi(i,2) = t(i);
        psi(i,3) = t(i)^2;
        psi(i, 4) = t(i) ^ 3;
    end 
    matr = psi;
end
function vec_matr = convert_vec_to_matr(vec)
    n = length(vec);
    vec_matr_loc = [];
    for i=1:n
        vec_matr_loc(i,1) = vec(i);
    end
    vec_matr = vec_matr_loc;
end
function teta = compute_teta(y, t)
    psi = create_matrix(t); 
    transposed_psi = transpose(psi);
    matr = inv(transposed_psi * psi) * transposed_psi;
    teta = matr * convert_vec_to_matr(y);
end
