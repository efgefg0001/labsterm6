; задание 2 функционал
(defun task-2-fun (lst)
    (if (should-mul2 lst)
        (mapcar #'(lambda (arg)
                    (if (numberp arg)
                        (* arg 2)
                        arg
                    )
                  )
                lst
        )
        (mapcar #'(lambda (arg)
                    (if (numberp arg)
                        (- arg 10)
                        arg
                    )
                  )
                lst
        )       
    )
)

(defun should-mul2 (lst)
    (let (
            (num 0)
            (sum 0)
            (result NIL)
        )
        (mapcar #'(lambda (arg)
                      (cond ((and (numberp arg) (< num  2)) (setf sum (+ sum arg) num (+ num 1)))
                            ((and (= num 2) (> sum  10)) (setf result T))
                            (T arg)
                      )
                  )
            lst
        )        
        result
    )
)

; задание 2 рекурсия 
(defun task-2-rec (lst)
    (if (should-mul2 lst)
        (mul2 lst)
        (dec10 lst)
    )
)
(defun mul2 (lst)
    (cond 
        ((null lst) NIL)
        ((numberp (car lst)) (cons (* (car lst) 2) (mul2 (cdr lst))))
        (T (cons (car lst) (mul2 (cdr lst))))
    )
)
(defun dec10 (lst)
    (cond 
        ((null lst) NIL)
        ((numberp (car lst)) (cons (- (car lst) 10) (dec10 (cdr lst))))
        (T (cons (car lst) (dec10 (cdr lst))))
    )
)

(defun should-mul2 (lst)
    (let (
            (num 0)
            (sum 0)
            (result NIL)
        )
        (mapcar #'(lambda (arg)
                      (cond ((and (numberp arg) (< num  2)) (setf sum (+ sum arg) num (+ num 1)))
                            ((and (= num 2) (> sum  10)) (setf result T))
                            (T arg)
                      )
                  )
            lst
        )        
        result
    )
)

; задание 3 функционал 
(defun task-3-fun (lst1 lst2)
    (let (
            (set1 (list-to-set (run-transform lst1)))
            (set2 (list-to-set (run-transform lst2)))
           
         )
        (set-difference set1 set2)
    )
)

; задание 3 рекурсия
(defun task-3-rec (lst1 lst2)
    (let* (
            (set1 (list-to-set-rec (run-transform lst1) () ))
            (set2 (list-to-set-rec (run-transform lst2) () ))
           
         )
        (set-difference set1 set2)
    )
)
(defun transform (lst rst)
    (cond ((null lst) rst)
          ((atom lst) (cons lst rst))
          (t (transform (car lst)
                        (transform (cdr lst) rst)))
    )
)
(defun run-transform (lst)
    (transform lst ())
)
(defun consist-of (lst)
    (if (member (car lst) (cdr lst))
        T 
        NIL
    )
)
(defun last-el (lst)
    (if (null (consist-of lst))
        (list (car lst))
    )
)
(defun list-to-set (lst)
    (mapcon #'last-el lst)
)

(defun list-to-set-rec (lst rst)
    (cond 
        ((null lst) rst)
        (T (if (not (member (car lst) rst))
               (list-to-set-rec (cdr lst) (cons (car lst) rst))
               (list-to-set-rec (cdr lst) rst)
           )
        )
    )
)

