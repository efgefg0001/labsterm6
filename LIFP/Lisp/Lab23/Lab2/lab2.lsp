(defun gipotenuza (a b)
    (sqrt (+ (* a a) (* b b)))
)

(defun V (a b c)
    (* a b c)
)

(defun longer_then (list_a list_b)
    (> (length list_a) (length list_b))
)

(defun mystery (x)
    (list (second x) (first x))
)

(defun f-to-c (temp)
    (* (/ 5.0 9.0) (- temp 320))
)

(defun katet (gipot kat_a)
    (sqrt (-(* gipot gipot) (* kat_a kat_a)))
)

(defun trapeciya (a b h)
    (* (+ a b) (/ h 2.0))
)
