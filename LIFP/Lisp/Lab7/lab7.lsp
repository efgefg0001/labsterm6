; задание 1
(defun memberp (elem lst)
    (dolist (item lst)
        (if (equalp elem item)
            (return T)
        )
    )
)

; задание 2
(defun it-assoc (elem lst)
	(dolist (item lst)
		(if (equalp (car item) elem)
			(return item)
		)
	)
)

; задание 3
(defun it-length (lst)
	(do (
                (item lst (cdr item)) 
                (i 0 (+ i 1))
            )
	    ((null item) i)
	)
)

; задание 4
(defun it-nth (index lst)
    (dotimes (i index (car lst))
        (pop lst)
    )
)

; задание 5
(defun it-reverse (lst)
    (let (
            (result ())
        )
	(dolist (item lst)
	    (push item result))
	result
    )
)

; задание 6
(defun it-union (x y)
	(let ((result (copy-seq x)))
		(dolist (item y)
			(if (not (member item result))
				(push item result)
			)
		)
		result
	)
)

(defun it-difference (x y)
    (let ((result ()))
        (dolist (item x result)
            (if (not (member item y))
                (push item result)
            )
        )
    )
)

(defun it-symmetric-difference (x y)
	(it-union (it-difference x y) (it-difference y x))
)

; задание 7
(defun find-largest-dolist (list-of-numbers)
    (let (
            (largest (car list-of-numbers))
        )
        (dolist (item (cdr list-of-numbers) largest)
            (if (> item largest)
                (setf largest item)
            )
        )
    )
)

(defun find-largest-do (list-of-numbers)
    (do*(
            (largest (car list-of-numbers))
            (tail (cdr list-of-numbers) (cdr tail))
            (item (car tail) (car tail))
        )
        ((null tail) largest)
        (if (> item largest)
            (setf largest item))
    )
)

; задание 8
(defun fnn-do (lst)
    (do*(
            (tail lst (cdr tail))
            (item (car tail) (car tail))
        )
        ((null tail) ’none)
        (if (not (numberp item))
            (return item)
        )
    )
)

(defun fnn-dolist (lst)
    (dolist (item lst ’none)
        (if (not (numberp item))
            (return item)
        )
    )
)

(defun fnn-rec (lst) 
    (cond 
        ((null lst) nil)
        ((numberp (car lst)) (fnn-rec(cdr lst)))
        (T (car lst))
    )
)

; задание 9
(defun sort-asc (lst) 
    (if (null (cdr lst)) 
        lst 
        (if (< (car lst) (cadr lst)) 
            (cons (car lst) (sort-asc (cdr lst))) 
            (cons (cadr lst) (sort-asc (cons (car lst) (cddr lst)))) 
        )
    )
)

(defun quicksort (list &aux (pivot (car list)) )
  (if (cdr list)
      (nconc (quicksort (remove-if-not #'(lambda (x) (< x pivot)) list))
             (remove-if-not #'(lambda (x) (= x pivot)) list)
             (quicksort (remove-if-not #'(lambda (x) (> x pivot)) list)))
      list))

(defun sort-asc-do (lst) 
	(do ((i (- (length lst) 1) (- i 1)))
		((= i 0))
		(do ((j 0 (+ j 1)))
			((= j i))
			(if (< (nth i lst) (nth j lst))
				(rotatef (nth i lst) (nth j lst))
			)
		)
	)       
	lst
 )

(defun bsr (L) 
    (if (null (cdr L)) 
        L 
        (if (< (car L) (cadr L)) 
            (cons (car L) (bsr (cdr L))) 
            (cons (cadr L) (bsr (cons (car L) (cddr L)))) 
        )
    )
)

(defun bubble-up (L)  
   (if (null (cdr L))  
     L   
  (if (< (car L) (cadr L))  
(cons (car L) (bubble-up (cdr L)))  
(cons (cadr L) (bubble-up (cons (car L) (cddr L))))  
  )
 )  
)
(defun bubble-sort-aux (N L)   
  (cond ((= N 1) (bubble-up L))  
        (T (bubble-sort-aux (- N 1) (bubble-up L)))))  

(defun bubble-sort-rec (lst)
    (bubble-sort-aux (length lst) lst)
)
