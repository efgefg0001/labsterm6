; факториал с функционалами
(defun fact (n)
        (apply #'* (get-list n))
)

(defun get-list (n)
  (let ((res '()))
    (if (= n 0)
        (list 1)        
        (append res (list n) (get-list (- n 1)))
    )
  )
)

; факториал с рекурсией
(defun fact-rec (num)
    (cond 
        ((= num 0) 1)
        (T (* num (fact-rec (- num 1))))
    )
)

; фибоначчи с рекурсией
(defun fib-rec (num)
    (cond 
        ((= num -1) 0)
        ((= num 0) 1)
        (T (+ (fib-rec (- num 1)) (fib-rec (- num 2))))
    )
)
