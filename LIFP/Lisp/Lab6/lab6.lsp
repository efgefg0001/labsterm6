; задание 2
(defun minus-ten (lst)
    (mapcar
        #'(lambda (elem)
	    (cond
	        ((numberp elem) (- elem 10))
		(T elem)
	    )
        )
        lst
    )
)

; задание 3
(defun get-arg (lst)
    (cond 
        ((and (listp (car lst)) (not (null (car lst)))) (car lst))
	(T (get-arg (cdr lst)))
    )
)

; задание 4
(defun between-a-b (lst a b)
    (remove nil 
        (mapcar
	    #'(lambda (elem)
	        (if (and (numberp elem) (< elem b) (> elem a)) 
                    elem
		)			
	    )
            lst
        )
    )
)

; задание 6
(defun decart-mult (A B)
    (mapcan #'
        (lambda (A)
            (mapcar 
                #'(lambda (B)
		    (list A B)
                )
		B
            )
        )
	A
    )
)

; задание 7
(defun length-list-of-lists (lst)
	(cond
		((null lst) 0)
		((listp lst)(+ (length-list-of-lists (car lst))(length-list-of-lists (cdr lst))))
		((numberp lst) 1)
		(T 0)
	)
)
; задание 8
(defun rec-add (lst )
  (let ((sum 0))
    (cond 
        ((endp lst) sum)
        ((listp (car lst)) (+ sum (rec-add(car lst))))
	((numberp (car lst)) (+ sum (car lst) (rec-add (cdr lst))))
	(T (rec-add (cdr lst)))
;        ((null lst) sum)
    )
  )
)

(defun rec-addm (L)
	(if L
		(+ 
			(cond
				((numberp (car L)) (car L))
				((listp (car L)) (rec-addm (car L)))
				(T 0)
			)	
			(rec-addm (cdr L))
		)
		0
	)
)

; задание 9
(defun rec-nth (num lst)
    (cond
        ((null lst) nil)
	((= num 0) (car lst))
	(T (rec-nth (- num 1) (cdr lst)))
    )
)

; задание 10
(defun alloddr (lst)
    (or 
        (null lst)
	(and 
	    (numberp (car lst))
	    (oddp (car lst))
	    (alloddr (cdr lst))
	)
    )
)

; задание 11
(defun tail-rec (lst)
    (if (null (cdr lst))
        (car lst)
	(tail-rec (cdr lst))
    )
)

; задание 12
(defun sum-n (n lst)
    (if (or (null lst) (< n 0))
        0
	(+ (car lst)
            (sum-n (- n 1) (cdr lst))
	)
    )
)

; задание 13
(defun last-odd (lst)
    (cond
        ((null lst) nil)
	((oddp (car lst)) 
	    (or 
	        (last-odd (cdr lst))
	        (car lst)
	    )
	)
	(T (last-odd (cdr lst)))
    )
)

; задание 14
(defun list-of-squares (lst)
    (and
        (not (null lst))
	(let ((x (car lst)))
	    (cons
	        (* x x)
		(list-of-squares (cdr lst))
	    )
	)
    )
)

(defun struct-of-squares (L)
	(if L
		(cons 
			(cond
				((numberp (car L)) (* (car L) (car L)))
				((listp (car L)) (list-of-squares (car L)))
				(T 0)
			)	
			(list-of-squares (cdr L))
		)
	)
)


; задание 15
; один уровень
(defun select-odd (lst)
	(remove-if-not #'oddp
		(remove-if-not #'numberp lst)
	)
)

(defun select-even (lst)
	(remove-if-not #'evenp
		(remove-if-not #'numberp lst)
	)
)

(defun sum-all-odd (lst)
    (apply #'+
        (select-odd lst)		
    )
)

(defun sum-all-even (lst)
    (apply #'+
        (select-even lst)
    )
)

; многоуровневый список
(defun sum-all-even (L)
	(if L
		(+ 
			(cond
				((numberp (car L)) (if (evenp (car L)) (car L) 0) )
				((listp (car L)) (sum-all-even (car L)))
				(T 0)
			)	
			(sum-all-even (cdr L))
		)
		0
	)
)

(defun all-even (L)
	(remove-if #'(lambda (x) (null x))
		(if L
			(cons 
				(cond
					((numberp (car L)) (if (evenp (car L)) (car L)))
					((listp (car L)) (remove-if #'(lambda (x) (null x)) (all-even (car L))))
					(T 0)
				)	
				(remove-if #'(lambda (x) (null x)) (all-even (cdr L)))
			)
		)
	)
)
