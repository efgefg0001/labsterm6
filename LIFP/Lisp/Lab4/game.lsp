(defun dice () 
	(+ (random 6) 1)
)

(defun roll () 
	(list (dice) (dice))
)

(defun reroll (lst) 
	(or (equal lst '(1 1)) (equal lst '(6 6)))
)

(defun turn () 
	(let (
                (first (roll))
            ) 
	    (if (reroll first) 
	        (roll) 
		first
	    )
	)
)

(defun summ-roll (lst) 
	(+ (car lst) (cadr lst))
)

(defun win (lst) 
	(let (
	        (sum (summ-roll lst))
	    ) 
	    (or (= sum 11) (= sum 7))
	)
)


(defun who-win (roll1 roll2) 
	(cond 
	    ((win roll1) "Player 1 won absolutely")
	    ((win roll2) "Player 2 won absolutely")
	    ((> (summ-roll roll1) (summ-roll roll2)) "Player 1 won")
	    ((< (summ-roll roll1) (summ-roll roll2)) "Player 2 won")
	    (T "Dead heat")
	)
)

(defun game () 
    (let (
	    (roll1 (turn)) 
  	    (roll2 (turn))
   	) 
	(print (list roll1 roll2 (who-win roll1 roll2)))
    )
)
