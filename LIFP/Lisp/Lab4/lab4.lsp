(defun last_arg_1 (lst)
    (car (reverse lst))
)

(defun last_arg_2 (lst)
    (cond ((<= (length lst) 1) (car lst))
          (T (last_arg_2 (cdr lst)))
    ) 
)

(defun last_arg_3 (lst)
   (if (> (length lst) 1)
        (last_arg_3 (cdr lst))
        (car lst)
    )
)

(defun list_without_last_1 (lst)
    (reverse (cdr (reverse lst)))
)

(defun list_without_last_2 (lst)
    (cons (car lst)
        (if (> (length (cdr lst)) 1)
            (list_without_last_2 (cdr lst))
            NIL
        )
    )
)

(defun list_without_last_3 (lst)
    (cons (car lst)
        (cond ((<= (length (cdr lst)) 1) NIL)
              (T (list_without_last_3 (cdr lst)))
        )
    )
)


