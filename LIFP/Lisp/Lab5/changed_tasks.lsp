; задание 2 равенство множеств
(defun set-equal (a b)
    (cond
        ((/= (my-length a) (my-length b)) NIL)
        (T
            (every #'(lambda (arg) (eql arg T)) 
                (mapcar #'(lambda (arg_a) 
                            (some #'(lambda (arg_b) (eql arg_a arg_b)) b)
                        ) 
                        a
                )
            )
        )
    )
)

(defun my-length (lst)
    (length lst)
)

; задание 8
(defun select-between (lst lower upper)
    (remove-if-not 
	#'(lambda (item)
	    (and (numberp item) (< item upper) (> item lower))
	)
        lst
    )
)

