(defun my-length (l)
	(let ((count 0))
		(mapcar 
			#'(lambda (x)
				(setf count (+ count 1))
			)
			l
		)
		count
	)
)

(defun my-equal (l1 l2)
	(let ((result T))
		(cond 
			(	
				(= (my-length l1) (my-length l2)) 
				(mapcar
					#'(lambda (x1 x2)
						(cond ((not (eql x1 x2)) (setf result nil))
						)
					)
					l1 l2
				)
			)
			(T (setf result nil))
		)
		result
	)
)

(defun my-reverse (l)
	(let ((new-list ()))
		(mapcar 
			#'(lambda (x)
				(setf new-list (cons x new-list))
			)
			l
		)
		new-list
	)
)

(defun my-nth (index lst)
	(let 
		(
			(res NIL)
			(i 0)
		)
		(mapcar 
			#'(lambda (arg)
				(cond
					((= i -1) arg)
					((= i index) (setf res arg i -1))
					((< i index) (setf i (+ i 1)))
				)
			)
			lst
		)
		res
	)
)

(defun my-nthcdr (index lst)
    (let 
		(
			(res lst)
			(i 0)
        )
        (mapcar 
			#'(lambda (arg)
				(cond 
					((= i -1) arg)
					((= i index) (setf i -1))
					((< i index) (setf i (+ i 1) res (cdr res)))
				)
			)
            lst
        )
        res
    )
)


(defun task1 (lst)
	(my-equal lst (my-reverse lst))
)

(defun task2 (a b)
    (cond
        ((/= (my-length a) (my-length b)) NIL)
        (T
            (every #'(lambda (arg) (eql arg T)) 
                (mapcar 
					#'(lambda (arg_a) 
						(some #'(lambda (arg_b) (eql arg_a arg_b)) b)
					) 
					a
                )
            )
        )
    )
)

;3ье задание
(setf CapitalsTable '((Russia . Moscow)(Australia . Canberra)))

(defun get_capital (country table)
	(cdr (assoc country table))
)

(defun get_country (capital table)
	(car (rassoc capital table))
)

(defun task4 (l)
	(let ((end (car (last l))))
		(rplaca
			(nconc
				(delete-if #'(lambda (x) (eq x end)) l)
				(cons (car l) nil)
			)
			end
		)
	)
)

(defun task5 (lst i1 i2)
    (let* 
		(
            (el1 (my-nth i1 lst))
            (el2 (my-nth i2 lst))
        )
        (rplaca (my-nthcdr i1 lst) el2)
        (rplaca (my-nthcdr i2 lst) el1)
        lst
    )
)

;6ое задание
(defun swap-to-left (lst)
    (let (
            (head_el (car lst))
        )
        (maplist #'(lambda (arg)
                        (rplaca arg (cadr arg))) 
                 lst)
        (rplaca (last lst) head_el)
        lst
    )
)

(defun swap-to-right (lst)
    (let 
		(
            (last_el (car (last lst)))
            (head_el (car lst))
        
            (new_lst (car (maplist #'(lambda (arg)
                            (cond
                                ((>=(my-length arg)2)(rplaca arg (cadr arg)))
                                (T arg)
                            )
                     )
                    (my-reverse lst))))
        )
        (maplist #'(lambda (lst1 lst2)
                        (rplaca lst1 (car lst2))
                    ) 
                 lst (rplaca (my-reverse new_lst) last_el))
        lst
    )
)

;7ое задание 
(defun mul-num (num lst)
    (mapcar 
        #'(lambda (elem)
                (* num elem)
        )
        lst
    )
)

(defun mul-obj (num lst) 
    (mapcar 
        #'(lambda (elem)
            (cond 
                ((numberp elem) (* num elem))
                (T elem)
            )
        )
        lst
    )
)

(defun task8 (lst lower upper)
    (remove-if-not 
		#'(lambda (item)
			(and (numberp item) (< item upper) (> item lower))
		)
        lst
    )
)


;Расчёт налога с использованием рекурсии
(defun mult-lst (x) 
    (* (car x) (car (last x)))
)

(defun sum-lst (lst)
    (if (= (length lst) 1)
        (car lst)
        (+ (car lst) (sum-lst (cdr lst)))
    )
)

(defun sum-and-mult-lst (lst)
    (if (= (length lst) 2)
        (* (first lst) (second lst))
        (+ (* (first lst) (second lst)) (sum-and-mult-lst (nthcdr 2 lst)))
    )
)

(defun nalog (table)
(mapcar #'(lambda (x) 
(list (car x) (sum-lst (mapcar #'sum-and-mult-lst (cdr x))))) table
)
)

(setf table1 
	'(
		(owner1 
			(1000 0.1) 
			(3000 0.2)
		) 
		(owner2 
			(7000 0.1)
		) 
		(owner3 
			(5000 0.5)
			(
				10000 0.05
				7000 0.1
				20000 0.2
			)
		)
	)
)


;Расчёт налога без рекурсии
(defun nalog-without-rec (table)
	(mapcar 
		#'(lambda (x) 
			(list (car x) (reduce #'+ (mapcar #'sum-and-mult-lst-without-rec (cdr x))))
		) table
	)
)

(defun sum-and-mult-lst-without-rec (lst)
	(reduce #'+
	(mapcar 
		#'(lambda (arg)
			(* (car arg) (cadr arg))) lst
		)
	)
)

(setf table2 
	'(
		(owner1
			(
				(1000. 0.1)
			)
			(
				(3000. 0.2)
			)
		)
		(owner2
			(
				(7000. 0.1)
				(7000. 0.05)
			)
		)

		(owner3
			(
				(5000. 0.5)
			)
			(
				(10000. 0.05)
				(7000. 0.1)
				(20000. 0.2)
			)
		)
    )
)

