; Задание 7 
; Напишите функцию, которая умножает на заданное число-аргумент все
; числа из заданного списка-аргумента, когда
; a) все элементы списка --- числа,
; b) элементы списка --- любые объекты.
; a)
(defun mul-num (num lst)
    (mapcar 
        #'(lambda (elem)
                (* num elem)
        )
        lst
    )
)

; б)
(defun mul-obj (num lst) 
    (mapcar 
        #'(lambda (elem)
            (cond 
                ((numberp elem) (* num elem))
                (T elem)
            )
        )
        lst
    )
)
