(setf table1 '(
                (owner1
                    ((1000. 0.1))
                    ((3000. 0.2))
                )
                (owner2
                    (
                        (7000. 0.1)
                        (7000. 0.05)
                    )
                )

                (owner3
                    (
                        (5000. 0.5)
                    )
                    (
                        (10000. 0.05)
                        (7000. 0.1)
                        (20000. 0.2)
                    )
                )
             )
)
(defun nalog-without-rec (table)
        (mapcar #'(lambda (x) 
                      (list (car x) (reduce #'+ (mapcar #'sum-and-mult-lst-without-rec (cdr x))))
                  ) table
        )
)



(defun sum-and-mult-lst-without-rec (lst)
  (reduce #'+
    (mapcar #'(lambda (arg)
                  (* (car arg) (cadr arg))) lst)
  )
)
