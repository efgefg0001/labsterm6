; задание 5
; Напишите функцию swap-two-element, которая 
; переставляет в списке-аргументе два указанных своими 
; порядковыми номерами элемента в этом списке.
(load "nth_nthcdr.lsp")

(defun swap-two-element (lst i1 i2)
    (let* (
            (el1 (my-nth i1 lst))
            (el2 (my-nth i2 lst))
        )
        (rplaca (my-nthcdr i1 lst) el2)
        (rplaca (my-nthcdr i2 lst) el1)
;        (format t "полученный список = ~a" lst)
;        (eval 'lst)
        lst
    )
)

