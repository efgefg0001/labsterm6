
(load "lab5.lsp")
; Задание 6
; Напишите две функции, swap-to-left и swap-to-right, которые производят
; круговую перестановку в списке-аргументе влево и вправо, соответственно.

; головной элемент на последнее место 
(defun swap-to-left (lst)
    (let (
            (head_el (car lst))
        )
        (maplist #'(lambda (arg)
                        (rplaca arg (cadr arg))) 
                 lst)
        (nreverse (rplaca (nreverse lst) head_el))
;        (format t "полученный список = ~a" lst)
        lst
    )

)
(defun swap-to-l (lst)
    (let (
            (head_el (car lst))
        )
        (maplist #'(lambda (arg)
                        (rplaca arg (cadr arg))) 
                 lst)
        (rplaca (last lst) head_el)
        lst
    )
)
(defun sw-to-l (lst)
    (cond
        ((null lst) lst) 
        (T
            (let (
                    (el_num_1 (nth 1 lst))
                    (tail (nconc (nthcdr 2 lst) 
                                 (list (car lst))
                          )
                    )

                )
                (rplacd (rplaca lst el_num_1) tail)
            )
        )
    )
)
(defun sw-to-r (lst)
    (let (
            (last_el (car (last lst)))        
            (tail (my-reverse (cdr (my-reverse lst))))
        )
            (rplacd (rplaca lst last_el) tail)
    )
)
; хвостовой элемент на место головного 
(defun swap-to-right (lst)
    (let (
            (last_el (car (last lst)))
            (new_lst (car (maplist #'(lambda (arg)
                            (cond
                                ((>=(length arg)2)(rplaca arg (cadr arg)))
                                (T arg)
                            )
                     )
                    (reverse lst))))
        )
        (maplist #'(lambda (lst1 lst2)
                        (rplaca lst1 (car lst2))
                    ) 
                 lst (rplaca (reverse new_lst) last_el))
;        (format t "полученный список = ~a" lst)
;        (eval 'lst)
    )
)
