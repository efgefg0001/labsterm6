(defun my-nth (index lst)
    (let (
            (res NIL)
            (i 0)
        )
        (mapcar #'(lambda (arg)
                    (cond 
                        ((= i -1) arg)
                        ((= i index) (setf res arg i -1))
                        ((< i index) (setf i (+ i 1)))
                    )
                )
            lst
        )
        res
    )
)

(defun my-nthcdr (index lst)
     (let (
            (res lst)
            (i 0)
        )
        (mapcar #'(lambda (arg)
                    (cond 
                        ((= i -1) arg)
                        ((= i index) (setf i -1))
                        ((< i index) (setf i (+ i 1) res (cdr res)))
                    )
                )
            lst
        )
        res
    )
   
)
