/*****************************************************************************

		Copyright (c) My Company

 Project:  LAB3
 FileName: LAB3.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "lab3.inc"

domains
	property =
		auto(string Producer, symbol Color);
		flat(string City, string Street, integer Area);
		land(string Vicinity, integer Area).

predicates
	ownership(integer, string, property, symbol, integer, integer)
	taxrate(property, real)
	tax(string, property, real)
	tax_with_id(string, integer, real)
	full_tax(string, integer, real)
	person(string, integer)
	find_full_tax(string, real)
clauses
	% tax section
	taxrate(auto(_, _), 0.1).
	taxrate(flat("Moscow", _, _), 0.18).
	taxrate(flat(City, _, _), 0.15):-City<>"Moscow".
	taxrate(land(_, _), 0.2).
	
	% ownership database
	person("Ivan", 3).
	person("Petr", 2).
	person("Liza", 1).
	
	ownership(1, "Ivan", auto("Audi", white), purchase, 1999, 12000).
	ownership(2, "Ivan", flat("Moscow", "Tverskaya", 75), purchase, 1991, 75000).
	ownership(3, "Ivan", flat("Moscow", "Tverskaya", 80), purchase, 1991, 80000).
	

	ownership(1, "Petr", auto("BMW", black), inheritance, 2002, 8000).
	ownership(2, "Petr", land("Vladimir", 600), purchase, 1998, 40000).
	

	ownership(1, "Liza", flat("Kazan", "Lenina", 40), dotation, 1975, 25000).
	
	%tax predicate

	tax(Owner, Property, Tax) :- 
		ownership(_, Owner, Property, _, _, Price),
		taxrate(Property, Taxrate),
		Tax = Price * Taxrate.
	tax_with_id(Owner, Id, Tax) :-
		ownership(Id, Owner, Property, _, _, Price),
		taxrate(Property, Taxrate),
		Tax = Price * Taxrate.
	find_full_tax(Owner, FullTax) :-
		person(Owner, Count),
		full_tax(Owner, Count, FullTax).
		
        full_tax(_, 0, 0) :- !.
    	full_tax(Owner, Count, FullTax) :- 
        	tax_with_id(Owner, Count, Tax1),
        	NewCount = Count - 1,
        	full_tax(Owner, NewCount, FullTax1),
        	FullTax = FullTax1 + Tax1.



goal
%	tax("Ivan", _, Tax).
	find_full_tax("Ivan", FullTax).
%	find_full_tax("Petr", FullTax).
