domains
	numberlist = integer*
predicates
	bubble_sort(numberlist, numberlist)
	b_sort(numberlist, numberlist, numberlist)
	bubble(integer, numberlist, numberlist, integer)

clauses
	bubble_sort(List,Sorted):-b_sort(List,[],Sorted).
	b_sort([],Acc,Acc).
	b_sort([H|T],Acc,Sorted):-bubble(H,T,NT,Max),b_sort(NT,[Max|Acc],Sorted).
   
	bubble(X,[],[],X).
	bubble(X,[Y|T],[Y|NT],Max):- X>Y,bubble(X,T,NT,Max).
	bubble(X,[Y|T],[X|NT],Max):- X<=Y, bubble(Y,T,NT,Max).
	
goal
	bubble_sort([5, 4, 100, 3, -500, 10], Sorted).
