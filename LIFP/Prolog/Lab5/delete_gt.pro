% delete all elements greater than number
domains
	numberlist = integer*

predicates
	delete_gt(numberlist, integer, numberlist)

clauses
	delete_gt([], _, []) :- !.
	delete_gt([Head|Rest], X, L) :-
  		Head > X, !,
  		delete_gt(Rest, X, L).
	delete_gt([Head|Rest], X, [Head|L]) :-
  		delete_gt(Rest, X, L).

goal
	delete_gt([1, 10, 563, 7 , 4, -10, 100, -555], 5, Result).
