/*****************************************************************************

		Copyright (c) My Company

 Project:  RK
 FileName: RK.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "rk.inc"

domains
	pnode = node*.
	node = node(integer Data, pnode Left, pnode Right).
	i=integer.
	list=i*.
	tree=nul;tr(integer,tree,tree);num(integer)

predicates
	% FOR LISTS
	modify(list In, list Out)
	double(list In, list Out)
	decrease(list In, list Out)
	check(list L)
	spis(integer,integer,list)
	sort(list, list, list)
	get_max(list, integer, integer)
	get_max(list, integer)
	del_by_val(list, integer, list, list)
	newlist(list In, list Out)
	list1(list In, list Out)
	list2(list In, list Out)
	conc(list One, list Two, list Res)
	range_valid(integer, integer, integer)
	range_invalid(integer, integer, integer)
	get_list(list, integer, integer, list, list)
	get_list(list, integer, integer, list)
	sum_list(list, integer)
	sum_list(list, integer, integer)
	element(list,integer,integer)
	memberLIST(list,integer)
	count(list,integer).
	count(list,integer,integer).
	getielem(list,integer,integer).
	incl(integer,list,list).
	
	% FOR SETS
	contains(list, integer)
	sub(list, list, list)
	member(integer,list)
	insert(integer,list,list)
	check_item(integer,list)
	check_inclusion(list, list)
	newlistSET(list In, list Mn, list Res).
	delete(integer X, list In, list Out).
	max_elem_from_list(list List, integer Max_from_list).
	max_elem_from_list_optimized(list List, integer Previously_max, integer Max_from_list).
	max_from_2_numbers(integer First_number, integer Second_number, integer Max_number).
	merge(list, list, list)
	merge_sets(list, list, list)
	is_set(list)
	setList(list,list)
	length(list,integer,integer)
	intersect(list,list,list)

	
	% FOR TREES
	treeNode(pnode)
	doubleCnd(pnode, pnode)
	doubleNode(pnode, pnode)
	modifyNode(pnode)
	find(tree,integer,integer)
	sred(tree,integer)
	gen(integer,tree)
	ins(integer,tree,tree)
	get_max_tree_depth(pnode, integer, integer, integer)
	get_max_tree_depth(pnode, integer)	
	memberTREE(integer,tree)
	length(tree,integer)
	replace(integer,integer,tree,tree)
	sum(tree,integer)
	concTREE(tree,tree,tree)
	is_tree_member(integer,tree).
	add_tree(integer,tree,tree).
	tree_delete_min(tree,tree,integer).	
	tree_delete_max(tree,tree,integer).


clauses
	% FOR LISTS
	modify(In, Out):-check(In),double(In, Out),!.
	modify(In, Out):-decrease(In, Out).	
	check([A, B|_]):-A + B > 0.
	
	double([], []).
	double([Ih|It], [Oh|Ot]):-Oh = Ih * 2,double(It, Ot).
		
	decrease([], []).
	decrease([Ih|It], [Oh|Ot]):-Oh = Ih - 10,decrease(It, Ot).
	
	spis(N,N,[]):-!.
	spis(N,K,[A|T]):-A=K*2+1,K1=K+1,spis(N,K1,T).
	
	get_max([], Max, Max).
	get_max([H|T], Max, Out) :-Max >= H,get_max(T, Max, Out).
	get_max([H|T], Max, Out) :-Max < H,get_max(T, H, Out).
	get_max([H|T], Out) :-get_max(T, H, Out).
  
	del_by_val([], _, Cur, Cur).
	del_by_val([H|T], H, Cur, Out) :-del_by_val(T, H, Cur, Out).
	del_by_val([H|T], Val, Cur, Out) :-H <> Val,del_by_val(T, Val, [H|Cur], Out).
  
	sort([], Cur, Cur).  
	sort([H|T], Cur, Out) :- get_max([H|T], Max),del_by_val([H|T], Max, [], Res),Tmp = [Max|Cur],sort(Res, Tmp, Out).
	
	range_invalid(Left, Right, Arg):-Arg < Left.
	range_invalid(Left, Right, Arg) :-Arg > Right.
  
	range_valid(Left, Right, Arg) :-Arg >= Left,Arg <= Right.

	get_list([], Left, Right, Cur, Cur).
	get_list([H|T], Left, Right, Cur, Out) :-range_valid(Left, Right, H),Tmp = [H|Cur],get_list(T, Left, Right, Tmp, Out).
	get_list([H|T], Left, Right, Cur, Out) :-range_invalid(Left, Right, H),get_list(T, Left, Right, Cur, Out).
	get_list(List, Left, Right, Out) :-get_list(List, Left, Right, [], Tmp),sort(Tmp, [], Out).
	
	newlist(Old,Res) :- list1(Old, List1), list2(Old, List2), conc(List1, List2, Res).

	list1([],[]).
	list1([H|T], [H|T1]) :- H<0, list1(T,T1).
	list1([H|T], T1) :- H>0, list1(T,T1).

	list2([],[]).
	list2([H|T], [H|T1]) :- H>0, list2(T,T1).
	list2([H|T], T1) :- H<0, list2(T,T1).	

	conc([ ], L, L). 
	conc([H|T], L, [H|T1]) :- conc(T,L,T1). 
	
	sum_list([], ListSum, Sum) :- ListSum = Sum.
	sum_list([Head|Tail], ListSum, Sum) :- NewListSum = Head + ListSum,sum_list(Tail, NewListSum, Sum).
	sum_list([], 0).
	sum_list([Head|Tail], Sum) :- sum_list(Tail, Head, Sum).
	
	element([H|T],1,H):-!.
	element([H|T],I,X):-J=I-1,element(T,J,X).
	
	memberLIST([H|T],X):-X=H,!.
	memberLIST([H|T],X):-memberLIST(T,X).
	
	count([],0).
	count([H|T],C) :- C1=1,count(T,C1,C).
	count([],C1,C1).
	count([H|T],C1,C) :- C2=C1+1,count(T,C2,C).
	
	getielem([H|_],1,H) :- !.
	getielem([H|T],I,Elem) :- I1=I-1,getielem(T,I1,Elem).
	
	incl(H,T,[H|T]).
	
	% FOR SETS
	contains([X|_], X):-!.
	contains([_|T], X):-contains(T, X).	
	
	sub([], _, []).
	sub([Ah|At], B, Out):-contains(B, Ah),sub(At, B, Out),!.
	sub([Ah|At], B, [Ah|Ot]):-%Oh=Ah,
				sub(At, B, Ot).
				
	member(X,[H|T]):-X=H,!.
	member(X,[H|T]):-member(X,T).
	
	insert(X,L,L):-member(X,L),!.
	insert(X,L,[X|L]).
	
	check_item(H, [H|T]).
	check_item(X,[H|T]) :- X <> H, check_item(X,T).
  
	check_inclusion([],List).
	check_inclusion([ArgH|ArgT],List) :- check_item(ArgH,List), check_inclusion(ArgT,List).
	
	newlistSET(List, Mn, Res) :- max_elem_from_list(Mn, X), delete(X, List, Res).
	
	delete(_, [], []).
	delete(X, [X|T], T1) :- delete(X, T, T1).
	delete(X, [Y|T], [Y|T1]) :- X<>Y, delete(X, T, T1).
	
	%Seach for max element of list using tail recursion
	%-----------------------------------------------------------------------------------
	max_elem_from_list([X], X).
	
	max_elem_from_list([Head|Tail], Max_from_list) :-max_elem_from_list_optimized(Tail, Head, Max_from_list).
	
	max_elem_from_list_optimized([Head|Tail], Previously_max, Max_from_list) :- max_from_2_numbers(Head, Previously_max, Max_from_2_candidates),max_elem_from_list_optimized(Tail, Max_from_2_candidates, Max_from_list).
		
	max_elem_from_list_optimized([X], Previously_max, Max_from_list) :-max_from_2_numbers(X, Previously_max, Max_from_list).
	%-----------------------------------------------------------------------------------
	
	max_from_2_numbers(X, Y, X) :- X>=Y.
	max_from_2_numbers(X, Y, Y) :- Y>X.
	
	merge(A, B, R) :- is_set(A),is_set(B),merge_sets(A, B, R).
	merge_sets([], A, R) :- R = A.
	merge_sets([Head|A], B, [Head|R]) :- not (contains(A, Head)), not (contains(B, Head)),!,merge(A, B, R).
	merge_sets([Head|A], B, R) :- merge_sets(A, B, R). 
	is_set([]). 
	is_set([Head|Tail]) :- not (contains(Tail, Head)), is_set(Tail).
	
	setList([],[]). 
	setList([H|T],[H|T1]):-delete(H,T,T2),setList(T2,T1). 
	
	length([],L,L):-!.
	length([H|T],P,L):-PL=P+1,length(T,PL,L).
	
	intersect([],_,[]).
	intersect([H|T1],S2,[H|T]):-member(H,S2),!,intersect(T1,S2,T). 
        intersect([_|T],S2,S):-intersect(T,S2,S). 
	

				
	% FOR TREES
	treeNode([node(4,[node(2,[node(1,[], [])],[node(3,[], [])])],[node(7,[node(5,[],[node(6, [], [])])],[node(8, [], [])])])]).
	
	doubleCnd([], []).
	doubleCnd([node(Fdata, Fleft, Fright)], [node(Tdata, Tleft, Tright)]):-Fdata <= 5,Tdata = Fdata,Tleft = Fleft,doubleCnd(Fright, Tright).
	doubleCnd([node(Fdata, Fleft, Fright)], [node(Tdata, Tleft, Tright)]):-Fdata > 5,Tdata = 2 * Fdata,doubleCnd(Fleft, Tleft),doubleNode(Fright, Tright).
	
	doubleNode([], []).
	doubleNode([node(Fdata, Fleft, Fright)], [node(Tdata, Tleft, Tright)]):-Tdata = 2 * Fdata,doubleNode(Fleft, Tleft),doubleNode(Fright, Tright).
	modifyNode(Y):-treeNode(X),doubleCnd(X, Y).

	ins(X,nul,tr(X,nul,nul)). 
	ins(X,tr(X,L,R),tr(X,L,R)):-!. 
	ins(X,tr(K,L,R),tr(K,L1,R)):-X<K,!,ins(X,L,L1). 
        ins(X,tr(K,L,R),tr(K,L,R1)):-ins(X,R,R1).
        
        gen(0,nul):-!. 
	gen(N,T):-random(60,X),N1=N-1,gen(N1,T1),ins(X,T1,T).
	
	find(nul,0,0):-!.
	find(tr(X,L,R),N,S):-X>12,find(L,N,S),!.
	find(tr(X,L,R),N,S):-X<3,find(R,N,S),!.
	find(tr(X,L,R),N,S):-find(L,N1,S1),find(R,N2,S2),N=N1+N2+1,S=S1+S2+X.
	
	sred(T,A):-find(T,N,S),N>0,A=S/N,!.
	sred(T,0).
	
	get_max_tree_depth([], Max, Cur, Cur) :-Cur >= Max.
	get_max_tree_depth([], Max, Cur, Max) :- Cur < Max.
  
	get_max_tree_depth([node(_, LTree, RTree)], Max, Cur, Out) :-NewCur = Cur + 1,get_max_tree_depth(LTree, Max, NewCur, NewMax),get_max_tree_depth(RTree, NewMax, NewCur, Out).
	get_max_tree_depth(Tree, Out) :-get_max_tree_depth(Tree, 0, -1, Out).
	
	memberTREE(X,tr(X,_,_)):-!. 
	memberTREE(X,tr(_,L,_)):-memberTREE(X,L),!. 
	memberTREE(X,tr(_,_,R)):-memberTREE(X,R).
	
	length(nul,0).
	length(tr(_,L,R),N):-length(L,NL),length(R,NR),N=NL+NR+1.
	
	replace(_,_,nul,nul).
	replace(X,Y,tr(X,L,R),tr(Y,L1,R1)):-!,replace(X,Y,L,L1),replace(X,Y,R,R1). 
        replace(X,Y,tr(K,L,R),tr(K,L1,R1)):-replace(X,Y,L,L1),replace(X,Y,R,R1).
        
        sum (nul,0).
	sum(tr(X,L,R),N):-sum (L,N1),sum (R,N2),N=N1+N2+X.
	
        concTREE(T,nul,T):-!.
        concTREE(T1,tr(X2,L2,R2),RES):-concTREE(T1,L2,RES1),concTREE(RES1,R2,RES2),ins(X2,RES2,RES).
        
        is_tree_member(X,tr(X,_,_)) :- !.
	is_tree_member(X,tr(_,L,_)) :- is_tree_member(X,L),!.
	is_tree_member(X,tr(_,_,R)) :- is_tree_member(X,R),!.

	add_tree(X,nul,tr(X,nul,nul)) :- !. 
	add_tree(X,tr(Y,Left,Right),tr(Y,ResL,Right)) :- X < Y, add_tree(X,Left,ResL).
	add_tree(X,tr(Y,Left,Right),tr(Y,Left,ResR)) :- X >= Y, add_tree(X,Right,ResR).
	
	
	% FOR BINARY TREES
	tree_delete_min(tr(X,nul,R), R, X).	
	tree_delete_min(tr(K,L,R), tr(K,L1,R), X):- tree_delete_min(L, L1, X).

	tree_delete_max(tr(X,L,nul), L, X).	
	tree_delete_max(tr(K,L,R), tr(K,L,R1), X):- tree_delete_max(R, R1, X).
	
	

	
goal
%	gen(10,T). % Generate random tree
%	get_list([-1,3,2,10,5,9,7,-100,9,1,1,1,0,-9,12,11,10], -2, 10, X). % Get elements from list between -2 & 10
%	sum_list([2, 6, -1, 9], Sum). % find sum of elements in list
%	merge([3, 2, 1, 0, -1, 5], [1, 5, 3, 2, -1, 0, 4], R). % Merge sets
%	setList([1,2,3,3,2,4],R). % Get list from set
%	length([1,2,3,3,2,4],0,L). %Find length of set
%	intersect([1,2,3,3,4],[1,2,4],R). % SET: A intersect B
%	element([2,3,4,5,6],2,X). % Get element by number
%	memberLIST([2,3,4,5,6],2). % Find element in list
%	memberTREE(1,tr(2,tr(1,nul,nul),nul),length(tr(2,tr(1,nul,nul),nul),L). % Find member in tree
%	replace(1,10,tr(1,tr(2,tr(1,nul,nul), nul),nul),X). % Replace element 1 by element 10
%	sum(tr(1,tr(2,tr(1,nul,nul), nul),nul),X). %Sum of tree nodes
%	concTREE(tr(1,tr(2,tr(1,nul,nul), tr(3,nul,nul)),nul),tr(4,tr(2,tr(4,nul,nul), nul),nul),R). % concate trees
%	count([1,2,0,3,4,0,-1],C). %Count number of elements
%	getielem([2,2,0,7],4,E). %Get element by index (4)
%	incl(2,[1,4,5],X). %Include element(2) in list
%	is_tree_member(1,tr(5,tr(3,tr(2,nul,nul),tr(4,nul,nul)),tr(7,nul,nul))). %Finds integer in tree
%	add_tree(10,tr(5,tr(3,tr(2,nul,nul),tr(4,nul,nul)),tr(7,nul,nul)),tr(H,L,R)). % adds element to tree
%	tree_delete_min(tr(5,tr(3,tr(1,nul,tr(2,nul,nul)),tr(4,nul,nul)),tr(7,nul,nul)),T,X). %deletes min from binary tree
%	tree_delete_max(tr(5,tr(3,tr(2,nul,nul),tr(4,nul,nul)),tr(7,nul,nul)),T,X). %deletes max from binary tree

%	modify([1,-2,3,4,5,6], X). % 3. LISTS: if Sum(A[1] + A[2]) > 0 then ALL ELEMENTS MULTIPLY BY 2 else ALL ELEMENTS MINUS 10
%	spis(7,0,X). 4. LISTS: CREATE LLIST FROM 7 first odd numbers
%	sort([-1,3,2,10,5,9,7,-100,9,1,1,1,0,-9,12,11,10], [], X). %6. Sort list
%	newlist([1,-5,34,5,-8,9,-6,2,-7], Result). % 17. LIST: put all negative elements to the beginning without changing there order

%	sub([1,2,3,4,5,6], [2, 4, 6], X). % 3. SET: A\B
%	insert(6,[1,2,3,4],L).	%4. SET: Insert element into set
%	check_inclusion([2,3],[2,3,4]). %6. SET: First in subset of second
%	newlistSET([23,4,5,4,1,-7,8,6,5,23,-65,87,23], [5,6,23,1,-7,8], Result). % 17. SET: Delete from list (first) max element of set(second)

%	modifyNode(Y). % 3. TREE: if (Node > 10) then Node MYLTIPLY BY 2
%	sred(tr(5,tr(4,tr(3,nul,nul),nul),nul),A). %4. TREE: Find Sred of All elements between 3 and 12
%	get_max_tree_depth([node(0,[node(10,[node(20,[],[])],[node(21,[],[])])],[node(11,[],[node(24,[],[node(38,[node(4,[],[node(5,[],[])])],[])])])])],X). % 6. Find tree height

