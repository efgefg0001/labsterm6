﻿implement main
    open core

    class facts - phoneNumbers
        person : (integer64 Number, string SecondName, string FirstName, string Patronymic, string City).

    class predicates
        reconsult : (string FileName).
    clauses
        reconsult(FileName) :-
            retractFactDB(phoneNumbers),
            file::consult(FileName, phoneNumbers).

    clauses
        run():-
            console::init(),
            stdIO::write("Загрузка данных\n"),
            reconsult("..\\numbers.txt"),
            stdIO::write("\nperson(A, \"Иванов\", \"Иван\", \"Иванович\", \"Омск\")\n"),
            person(A, "Иванов", "Иван", "Иванович", "Омск"),
                stdIO::writef("A = %\n", A),
            fail.

        run():-
            stdIO::write("\nperson(89951753284, A, B, C, D)\n"),
            person(89951753284, A, B, C, D),
                stdIO::writef("A = %, B = %, C = %, D = %\n", A, B, C, D),
            fail.

        run():-
            stdIO::write("\nperson(A, \"Безногов\", B, C, D)\n"),
            person(A, "Безногов", B, C, D),
                stdIO::writef("A = %, B = %, C = %, D = %\n", A, B, C, D),
            fail.

        run():-
            stdIO::write("\nperson(A, B, C, D, \"Омск\")\n"),
            person(A, B, C, D, "Омск"),
                stdIO::writef("A = %, B = %, C = %, D = %\n", A, B, C, D),
            fail.

        run():-
            stdIO::write("\nperson(A, B, \"Ахмед\", C, D)\n"),
            person(A, B, "Ахмед", C, D),
                stdIO::writef("A = %, B = %, C = %, D = %\n", A, B, C, D),
            fail.

        run():-
            stdIO::write("\nperson(A, B, C, \"Иванович\", D)\n"),
            person(A, B, C, "Иванович", D),
                stdIO::writef("A = %, B = %, C = %, D = %\n", A, B, C, D),
            fail.

        run():-
            stdIO::write("\nКонец\n").
end implement main

goal
    mainExe::run(main::run).
