/*****************************************************************************

		Copyright (c) My Company

 Project:  LAB2
 FileName: LAB2.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "lab2.inc"

predicates

%  lab2()
	man(symbol)
	woman(symbol)
	parent(symbol, symbol, symbol)
	grand_parent(symbol, symbol, symbol)

clauses

%  lab2():-!.
	man(anton).
	man(pyotr).
	man(pavel).
	man(stepan).
	man(alexandr).
	man(vladimir).
	man(boris).
	man(vladislav).
	
	woman(masha).
	woman(eva).
	woman(katya).
	woman(svetlana).
	woman(anna).
	woman(vika).
	
	parent(anton, pyotr, m). % Anton - roditel' Pyotr
	parent(eva, pyotr, f).
	parent(anton, katya, m).
	parent(eva, katya, f).	parent(vladimir, pavel, m).
	parent(masha, pavel, f).
	parent(pyotr, stepan, m).
	parent(svetlana, stepan, f).
	parent(katya, boris, f).
	parent(alexandr, boris, m).
	parent(vika, alexandr, f).
	parent(vladislav, alexandr, m).
	
	grand_parent(G, GC, Pol):-
		parent(G, P, Pol), parent(P, GC, _).
	
%	grand_mother(G, GC):-
%		woman(G), grand_parent(G, GC).
	
%	grand_father(G, GC):-
%		man(G), grand_parent(G, GC).
	

goal

%  lab2().
%	father(F, pavel).
%	mother(M, pavel).
%	son(S, anton).
%	daughter(D, eva).
%	grand_parent(G, stepan).
%	grand_parent(G, boris).
%	grand_mother(G, stepan).
%	grand_father(G, stepan).
%	grand_mother(G, boris).
	grand_parent(Name, boris, f).
  
