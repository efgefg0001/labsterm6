domains
	numberlist = integer*
	maxElem = integer

predicates

	max(numberlist, maxElem) 
	max_tail(numberlist, integer, integer)
	run_max(numberlist, integer)

clauses
	max([X],X).
	max([H|Tail],H):-max(Tail,M),H>M, !.
	max([_|Tail],M):-max(Tail,M).
	
	max_tail(List, Temp, Max) :-
		Max = Temp,
		write(Max), !.
	run_man([Head|List], Max) :-
		max_tail(List, Head, Max).

goal
%	max([10, 1, 5, 300, 4, 10], Max).
	run_max([10, 1, 5, 300, 4, 10], Max)