domains
	numberlist = integer*
	elem = integer

predicates

	member(elem, numberlist) 

clauses
	member(Elem, [Elem|Tail]):-!.
	member(Elem, [Head|Tail]):-
		member(Elem, Tail).

goal
	member(3, [1, 2, 3, 4]).