domains
	numberlist = integer*

predicates

	sum_list(numberlist, integer) 
	run_sum(numberlist, integer)
	sum_tail(numberlist, integer, integer)

clauses
	sum_list([], 0):-!.
	sum_list([Head|Tail], Sum) :- 
		sum_list(Tail, TailSum),
		Sum = TailSum + Head.
	
	run_sum(List, Res):-
		sum_tail(List, 0, Res).
	sum_tail([], Temp, Temp):- !.
	sum_tail([Head|Tail], Temp, Sum):-
		Temp1 = Temp + Head,
		sum_tail(Tail, Temp1, Sum).
	

goal
%	sum_list([1, 2, 3, 4], Sum).
	run_sum([1, 2, 5, 4, 1000], Sum).