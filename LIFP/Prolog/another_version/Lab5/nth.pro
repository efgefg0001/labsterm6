domains
	numberlist = integer*
	index = integer
	elem = integer

predicates

	nth(index, numberlist, elem) 

clauses
	nth(0, [Elem|_Tail], Elem):-!.

	nth(Index, [_Head|Tail], Elem):-
		NextIndex = Index - 1,
		nth(NextIndex, Tail, Elem).

goal
	nth(-44, [1, 2, 3, 4], Elem).