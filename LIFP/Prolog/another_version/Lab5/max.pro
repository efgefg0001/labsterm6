domains
	numberlist = integer*
	maxElem = integer

predicates

	max(numberlist, maxElem) 

clauses
	max([MaxElem], MaxElem):-!.
	max([Head|Tail], MaxElem):-
		max(Tail, TailMaxElem),
%		TailMaxElem > Head, !, MaxElem = TailMaxElem; MaxElem = Head.
		TailMaxElem > Head, MaxElem = TailMaxElem.
	max([Head|Tail], MaxElem):-
		max(Tail, TailMaxElem),
		TailMaxElem <= Head,
		MaxElem = Head.

goal
	max([1, 5, 3, 4, 10], Max).