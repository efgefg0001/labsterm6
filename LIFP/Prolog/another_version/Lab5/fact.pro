predicates
	fact(real,real).
	f(real, real).	
	f(real, real, real, real).
	
clauses
	fact(0,1).
	fact(N,F) :- N > 0, N1 = N - 1, fact(N1, F1), F = F1 * N.
	

	f(N,F) :- f(0,1,N,F).

	f(N,F,N,F) :- !.
	f(I,Temp,N,F) :- I1 = I+1, Temp1 = Temp*I1, f(I1,Temp1,N,F).

goal
	%fact(5,X).
	f(100, X).
