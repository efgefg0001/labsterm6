/*****************************************************************************

		Copyright (c) My Company

 Project:  LAB5
 FileName: QSORT.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/


domains
	numberlist = integer*
predicates
	quick_sort(numberlist, numberlist)
	pivoting(integer, numberlist, numberlist, numberlist)
	append(numberlist, numberlist, numberlist)
	run_append(numberlist, numberlist)
clauses
	pivoting(H,[],[],[]).
	pivoting(H,[X|T],[X|L],G):-X<H,pivoting(H,T,L,G).
	pivoting(H,[X|T],L,[X|G]):-X>=H,pivoting(H,T,L,G).
	run_append(L1, L2):- 
		append(L1, [], L2).
	append([],L,L). 
	append([H|T],L,[H|LT]):-
		append(T,L,LT). 
	quick_sort([],[]).
	quick_sort([H|T], Sorted):-
		pivoting(H,T,L1,L2),quick_sort(L1,Sorted1),quick_sort(L2,Sorted2),
		append(Sorted1,[H|Sorted2], Sorted1).


goal
	quick_sort([1, 500, -400, 1000, 900, 800, 10000], Result).

