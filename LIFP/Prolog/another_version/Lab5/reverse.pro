domains
	numberlist = integer*

predicates

	reverse(numberlist, numberlist) 
	reverse(numberlist, numberlist, numberlist) 

clauses
	reverse(List, ReverseList):-
		reverse(List, [], ReverseList). 
 
	reverse([], Buffer, Buffer):-!.
	reverse([Head|Tail], Buffer, ReverseList):-
		reverse(Tail, [Head|Buffer], ReverseList).

goal
	reverse([1, 5, 3, 4], ReverseList).