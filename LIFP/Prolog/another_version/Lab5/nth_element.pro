% find nth element of a list
% indices begin with 1
domains
    numberlist = integer*
    
predicates
    find(numberlist, integer)
    
clauses

    find([],N) :-
        write("There is no such element in the list"),nl.
        
    find([Element|List],1) :-
        write("The element is ",Element),nl.
        
    find([Element|List],N) :-
        N1 = N-1,
        find(List,N1).

goal
	find([10, 9, 8, 7, 6, 5], 3). % returns 8