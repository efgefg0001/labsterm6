domains
	numberlist = integer*
	elem = integer

predicates

	delete(numberlist, elem, numberlist) 

clauses
	delete([], _Elem, []):-!.
	delete([Elem|Tail], Elem, ResultTail):-
		delete(Tail, Elem, ResultTail), !.
	delete([Head|Tail], Elem, [Head|ResultTail]):-
		delete(Tail, Elem, ResultTail).

goal
	delete([1, 5, 3, 5, 4, 6], 5, ResultList).