/*****************************************************************************

		Copyright (c) My Company

 Project:  LAB1
 FileName: LAB1.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "lab1.inc"

constants
	max_vklad = 10000

predicates
	vkladchik(string, string, integer)
%	max(integer, integer, integer)

clauses
	vkladchik(ivanov, sberbunk, 50000).
	vkladchik(ivanov, sberbunk, 5000).
	vkladchik(_, _, C) :- C > max_vklad.
%	max(A, B, A) :- A >= B.

goal
	vkladchik(A, B, 50000).
	