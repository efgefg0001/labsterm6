/*****************************************************************************

		Copyright (c) My Company

 Project:  LAB1
 FileName: LAB1.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "lab1.inc"

constants
	max_vklad = 10000

predicates
	vkladchik(string, string, integer)

clauses
	vkladchik(ivanov, sberbank, 50000).
	vkladchik(ivanov, sberbank, 5000).
%	vkladchik(_, _, C) :- C > max_vklad.
%	max(A, B, A) :- A >= B.

goal
	vkladchik(ivanov, sberbank, 50000).
	