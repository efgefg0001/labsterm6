domains
	tree= empty; tr(integer, tree, tree)

predicates
	tree_del_max(tree, tree, integer)

clauses
	tree_del_max(tr(X,L,empty), L, X).

	tree_del_max(tr(K,L,R), tr(K,L,R1), X):-
		tree_del_max(R, R1, X).

goal
	tree_del_max(
		tr(25, 
		  tr(20,
		  	tr(18,empty,empty),
		  	tr(21,empty,empty)), 
		  tr(33,
		    tr(36,empty,empty),
		    tr(39, empty, empty)
		  ) 
		), 
		X, Y).