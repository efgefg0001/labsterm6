domains
	tree= empty; tr(integer, tree, tree)

predicates
	tree_del_min(tree, tree, integer)

clauses
	tree_del_min(tr(X,empty,R), R, X).
	tree_del_min(tr(K,L,R), tr(K,L1,R), X):-
		tree_del_min(L, L1, X).

goal
	tree_del_min(
		tr(25, 
		  tr(20,
		  	tr(18,empty,empty),
		  	tr(21,empty,empty)), 
		  tr(33,
		    tr(36,empty,empty),
		    tr(39, empty, empty)
		  ) 
		), 
		X, Y).