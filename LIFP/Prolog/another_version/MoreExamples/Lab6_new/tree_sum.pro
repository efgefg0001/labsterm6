domains
	tree=empty; tr(integer, tree, tree)

predicates
	tree_sum(tree, integer)

clauses
	tree_sum(empty, 0).
	tree_sum(tr(X, L, R), Sum) :-
		tree_sum(L, Sum1),
		tree_sum(R, Sum2),
		Sum = Sum1 + Sum2 + X.

goal
			tree_sum(
			tr(2, 
		  		tr(7,empty,empty), 
		  		tr(3,
		    			tr(4,empty,empty),
		    			tr(1, empty, empty)
		  		) 
			), 
			Num).


