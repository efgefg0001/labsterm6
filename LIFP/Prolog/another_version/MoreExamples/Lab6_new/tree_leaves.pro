domains
	tree=empty; tr(integer, tree, tree)

predicates
	tree_leaves(tree, integer)

clauses

	tree_leaves(empty, 0).
	tree_leaves(tr(_, empty, empty), 1) :- !.
	tree_leaves(tr(_, L, R), N) :-
		tree_leaves(L, N1),
		tree_leaves(R, N2),
		N = N1 + N2.	

goal
		tree_leaves(
			tr(2, 
		  		tr(7,empty,empty), 
		  		tr(3,
		    			tr(4,empty,empty),
		    			tr(1, empty, empty)
		  		) 
			), 
			Num).


