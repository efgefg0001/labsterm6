domains
	elem = i(integer) ; l(list)
	list = elem*

predicates
	head_sum(elem, integer)
	list_sum(list, integer)
	
clauses
	head_sum(i(Num), Num).
	head_sum(l(List), S) :- list_sum(List, S).
	list_sum([], 0).
	list_sum([H|T], S) :- head_sum(H, S1), list_sum(T, S2), S = S1 + S2.

goal
	list_sum([i(1), l([i(2), l([i(3), i(7)]), i(8)]), i(3)], S).