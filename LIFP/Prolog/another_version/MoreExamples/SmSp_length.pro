domains
	elem = i(integer) ; l(list)
	list = elem*

predicates
	head_len(elem, integer)
	list_len(list, integer)
	
clauses
	head_len(i(_), 1).
	head_len(l(List), L) :- list_len(List, L).
	list_len([], 0).
	list_len([H|T], L) :- head_len(H, L1), list_len(T, L2), L = L1 + L2.

goal
	list_len([i(1), l([i(2), l([i(3), i(7)]), i(8)]), i(3)], L).