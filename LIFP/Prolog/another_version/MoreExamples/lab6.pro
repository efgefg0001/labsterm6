/*****************************************************************************

		Copyright (c) My Company

 Project:  LAB6
 FileName: LAB6.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "lab6.inc"

domains
	list = elem*
	elem = l(list); i(integer).

predicates
	flatten(list, list)
	head_flatten(elem, list)
	
	append(list, list, list)
	
clauses
	head_flatten(i(Num), [i(Num)]).
	head_flatten(l(List), Flattened) :- flatten(List, Flattened).

	flatten([], []) :- !.
	flatten([L|Ls], FlatL) :-
	    !,
	    head_flatten(L, NewL),
	    flatten(Ls, NewLs),
	    append(NewL, NewLs, FlatL).
	
	append([], List2, List2) :- !.
	append([Head|Tail], List2, [Head|TailResult]) :-  
		append(Tail, List2, TailResult).

goal

  	flatten([i(2), i(3), l([i(3), i(4)])], X).
