domains
	tree=empty; tr(integer, tree, tree)

predicates
	tree_del_min(tree, tree, integer)

clauses
	tree_del_min(tr(X, empty, R), R, X).
