predicates
	fib(integer, integer, integer, integer)
	fibon(integer, integer)

clauses
	fib(0, A, _, A).
	fib(N, A, B, F) :- N> 0, N1 = N - 1, Sum = A + B, fib(N1, B, Sum, F).
	
	fibon(N, F) :- fib(N, 0, 1, F).

 
goal
	fibon(5, F).

