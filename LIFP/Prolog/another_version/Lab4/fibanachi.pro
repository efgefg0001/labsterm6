predicates
	fib(integer,integer)
clauses
      	fib(0,0).			
      	fib(1,1).			 
      	fib(N,F) :- N>0, 
        		N1=N-1,
                   	fib(N1,F1),
                   	N2=N-2,
                   	fib(N2,F2),
                   	F=F1+F2.
Goal
	fib(5,Y).