domains
	llist = l(list); i(integer)
	list = llist*
	general = l(list); i(integer)

predicates
	is_list(general)

clauses
	is_list(l([])).
	is_list(l([_|T])) :- 
		is_list(l(T)).

goal
%	is_list(l([i(1), l([i(5), i(6), l([i(7), i(8)])])])).
	is_list(i(5)).

