/*****************************************************************************

		Copyright (c) My Company

 Project:  LAB6
 FileName: LAB6.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "lab6.inc"

predicates

  lab6()

clauses

  lab6():-!.

goal

  lab6(), write("Hello\n").
