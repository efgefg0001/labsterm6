domains
	llist = ls(list); in(integer)
	list = llist*
	general = l(list); i(integer)
	
predicates
	is_list(general)
	run_sum(general, integer)
	sum_tail(general, integer, integer)

clauses
	is_list(l([])).
	is_list(l([_|T])) :- 
		is_list(l(T)).
	
	run_sum(List, Res):-
		sum_tail(List, 0, Res).
	sum_tail(l([]), Temp, Temp):- !.
	sum_tail(l([Head|Tail]), Temp, Sum):-
		Sum = 0,
		write(Head),
		write("\n").
%		is_list(Head).
%		is_list(Head),
%		sum_tail(Head, Temp, Sum).
		
%	sum_tail(l([Head|Tail]), Temp, Sum):-
%		not(is_list(Head)),
%		Temp1 = Temp + Head,
%		sum_tail(Tail, Temp1, Sum).


goal
	run_sum(l([in(1), in(2), in(3), in(4), in(5)]), Res).

