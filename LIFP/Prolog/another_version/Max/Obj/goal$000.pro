/*****************************************************************************

		Copyright (c) My Company

 Project:  MAX
 FileName: MAX.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "max.inc"

predicates

  max2(integer, integer, integer).
  max3(integer, integer, integer, integer).
  
clauses

  max2(X,Y,X) :- X>=Y.
  max2(X,Y,Y) :- X<Y.
  
  max3(X,Y,Z,X) :- max2(X, Y, X), max2(X, Z, X).
  max3(X,Y,Z,Y) :- max2(X, Y, Y), max2(Z, Y, Y).
  max3(X,Y,Z,Z) :- max2(X, Z, Z), max2(Y, Z, Z).


goal

  %max2(5,7, X).
  max3(1000000, 5555, 10, X).
