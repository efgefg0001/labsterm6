domains
	tree=empty; tr(integer, tree, tree)
predicates
	bin_tree_gen(integer, tree)
	bin_tree_insert(integer, tree, tree)
clauses
	bin_tree_insert(X, empty, tr(X, empty, empty)).
	bin_tree_insert(X, tr(X, L, R), tr(X, L, R)) :-!.
	bin_tree_insert(X, tr(K, L, R), tr(K, L1, R)) :-
		X<K,!,
		bin_tree_insert(X, L, L1).
	bin_tree_insert(X, tr(K, L, R), tr(K, L, R1)) :-
		bin_tree_insert(X, R, R1).
	
	bin_tree_gen(0, empty) :- !.
	bin_tree_gen(N, T) :-
		random(1000, X),
		N1 = N -1,
		bin_tree_gen(N1, T1),
		bin_tree_insert(X, T1, T).

goal
	bin_tree_gen(6, Tr).
		
	

