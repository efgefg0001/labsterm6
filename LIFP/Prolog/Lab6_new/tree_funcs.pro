domains
	tree=empty; tr(integer, tree, tree)

predicates
	tree_member(integer, tree)
	tree_length(tree, integer)
	run

clauses
	tree_member(X, tr(X,_,_)):-!.
	tree_member(X, tr(_, L, _)):-
		tree_member(X,L),!.
	tree_member(X, tr(_,_,R)):-
		tree_member(X,R).
	
	tree_length(empty, 0).
	tree_length(tr(_, L, R), N):-
		tree_length(L, N1),
		tree_length(R, N2),
		N = N1+N2+1.	
	run :-
		Tr = tr(2, 
		  tr(7,empty,empty), 
		  tr(3,
		    tr(4,empty,empty),
		    tr(1, empty, empty)
		  ) 
		),
		tree_member(17, Tr).
%		tree_length(Tr, Num).

goal
	run.


