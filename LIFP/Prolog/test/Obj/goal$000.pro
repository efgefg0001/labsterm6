domains
	tree=empty; tr(integer, tree, tree)

predicates
	tree_sum(tree, integer, integer, integer)

clauses
	tree_sum(empty, A, B, 0).
	tree_sum(tr(X, L, R), A, B, Sum) :-
		tree_sum(L, A, B, Sum1),
		tree_sum(R, A, B, Sum2),
		X >= A, X <=B, !,
		Sum = Sum1 + Sum2 + X.
		
	tree_sum(tr(X, L, R), A, B, Sum) :-
		tree_sum(L, A, B, Sum1),
		tree_sum(R, A, B, Sum2),
		Sum = Sum1 + Sum2.

goal
			tree_sum(
			tr(-100, 
		  		tr(7,empty,empty), 
		  		tr(3,
		    			tr(4,empty,empty),
		    			tr(1, empty, empty)
		  		) 
			), 
			1, 5,
			Sum).




