domains 
	list = integer*
predicates
	get_max(list, integer, integer)
	get_max(list, integer)
	count(list, integer, integer, integer)
	get_count_of_max(list, integer)

clauses
	get_max([], Max, Max).
	get_max([H|T], Max, Out) :-Max >= H,get_max(T, Max, Out).
	get_max([H|T], Max, Out) :-Max < H,get_max(T, H, Out).
	get_max([H|T], Out) :-get_max(T, H, Out).

	count([], Temp, El, Temp) :- !.
	count([H|T],Temp,El,C) :- H=El, Temp1 = Temp + 1,count(T,Temp1,El, C).
	count([H|T],Temp,El,C) :- H<>El,count(T,Temp,El, C).
	
	get_count_of_max(List, Count) :-
		get_max(List, Max),
		count(List, 0, Max, Count).

goal
	get_count_of_max([1, 2, 3, 3, 5, -100, 5, -1000, 5], Count).
