domains
	list=integer*

predicates
	delete_elem_of_set(list, list, list)
	delete(list, integer, list)

clauses
	delete([], Elem, []):-!.
	delete([Elem|Tail], Elem, ResultTail):-
		delete(Tail, Elem, ResultTail), !.
	delete([Head|Tail], Elem, [Head|ResultTail]):-
		delete(Tail, Elem, ResultTail).	
		
	delete_elem_of_set(List, [], List).
	delete_elem_of_set(List, [H|TailSet], Res) :-
		delete(List, H, Res1),
		delete_elem_of_set(Res1, TailSet, Res).
			

goal
	delete_elem_of_set([5, 2, 3, 4, 5, 6], [5, 3, 7], Res).
%	delete([5, 2, 3, 4, 5, 6], 5, Res).

